﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bodypart : MonoBehaviour 
{
	public float Health = 100;
	public float MaxHealth;
	public string Name;
	public string Verb;

	public bool Infected = false;
	public bool Bleeding = false;

	public static float BleedDamage = 5;
		
	public List<Item.ClothingSlot> ContainedSlots;

	public void TickBleedingAndInfection()
	{
		if (Bleeding) {
			if(Random.Range(0,100) > 10)
			{
				Health -= BleedDamage;
				Global.UIMaster.OutputBox.AddMessage("You lose blood from your " + Name,TextBoxI.MColor.Red);
			}
			else
			{
				Bleeding = false;
				Global.UIMaster.OutputBox.AddMessage("The wound in your " + Name + " clots",TextBoxI.MColor.Green);
			}
		}
	}

	public float CalulateAmour(Item.DamageTypes DamageType)
	{
		float amour = 0;
		foreach (Item.ClothingSlot Slot in ContainedSlots) {
			amour += Global.Player.CalculateAmour(Slot,DamageType,true);
		}

		return amour;
	}

}
