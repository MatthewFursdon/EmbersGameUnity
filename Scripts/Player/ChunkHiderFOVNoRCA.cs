﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class ChunkHiderFOVNoRCA : MonoBehaviour
 {
	Transform cameraT;
	Camera cam;
	public LayerMask ChunkHideLayerMask;
	// Use this for initialization
	void Start ()
	{
	
	cameraT = Camera.main.transform;
		cam = Camera.main;
			//InvokeRepeating("ShootRays",0,1);
			
			//StartCoroutine(ShootRays());
	}

	public Vector3 NextPointInBounds(GameObject Object,int Slices,int i)
	{
	//divide into slices
	//float SliceSize = (Block.BLOCK_SIZE*Chunk.CHUNK_SIZE)/(float)Slices;
	float axis = Mathf.Round(Mathf.Sqrt(Slices));
	
	float zS = Mathf.Round(i/axis)/axis;
	float xS = i-Slices*zS;
		
		//Vector3 RandPoint = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f));
		Vector3 RandPoint = Object.transform.TransformPoint(new Vector3(xS,Random.Range(-50,50)/100.0f,zS));
		return RandPoint;
	}

	
	public Vector3 RandomPointInBounds(GameObject Object)
	{
		Vector3 RandPoint = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f));
		RandPoint = Object.transform.TransformPoint(RandPoint * 0.5f);
		return RandPoint;
	}

	
	Vector3 oldRot;
	Vector3 OldPos;
	int delay;
	void LateUpdate()
	{
		Vector3 Latest = cameraT.rotation.eulerAngles;
		Vector3 Dr = Latest-oldRot;
		Vector3 Pos = cameraT.position;
		Vector3 Dp = Pos-OldPos;
		
		
		
		if(Mathf.Abs(Dr.y) > 2 || Mathf.Abs(Dr.z) > 2 || Dp.sqrMagnitude > 1 || delay > 100)
		{
		oldRot = Latest;
		OldPos = Pos;
		ShootRays();
		delay = 0;
		
		}
		
		delay++;
	}
	
	int i = 0;

	List<Chunk> chunks;
	int j = 0;
	Vector3 offset = new Vector3(0,1,0);
	Vector3 LastPos;
	public  void ShootRays()
	{

	
			//cam.fieldOfView = 90;
			float x = 0;
			float z = 0;
			
			float dx = (float)cam.pixelWidth/35.0f;
			float dz = (float)cam.pixelHeight/35.0f;
		
			while(x <= 35)
			{
			
			while(z <= 35)
			{
			
			//Ray ray = cam.ScreenPointToRay(new Vector3(dx*x,dz*z,0));
			Ray ray = cam.ScreenPointToRay(new Vector3(dx*x + Random.Range(-dx,dx),dz*z+ Random.Range(-dz,dz),0));
			
			RaycastHit result;
			
			if(Physics.Raycast(ray.origin,ray.direction.normalized,out result,20000,ChunkHideLayerMask))
			{
			//Debug.DrawRay(ray.origin,ray.direction.normalized*10000,Color.green);
			GameObject o = result.collider.gameObject;
				

				GameObject k = result.collider.transform.root.gameObject;
				Chunk other = k.GetComponent<Chunk>();
				if(other != null)
				{
						
						
					other.Visible = true;
					other.HideTime = 60;
					other.UpdateRenderStatus();
				}
				else
				{
				//Debug.Log(k.name);
				}
			
			}
			else
			{
			//Debug.DrawRay(ray.origin,ray.direction*10000,Color.red);
			}
			z++;
			}
			x++;
			z = 0;
		
		}
//yield return new WaitForSeconds(0.1f);
		j++;
		

			
	//cam.fieldOfView = 76;
		
	}

}
