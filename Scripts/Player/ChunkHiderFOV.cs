﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class ChunkHiderFOV : MonoBehaviour
 {
	Transform cameraT;
	Camera cam;
	// Use this for initialization
	void Start ()
	{
	
	cameraT = Camera.main.transform;
		cam = Camera.main;
			//InvokeRepeating("ShootRays",0,1);
			
			//StartCoroutine(ShootRays());
	}

	public Vector3 NextPointInBounds(GameObject Object,int Slices,int i)
	{
	//divide into slices
	//float SliceSize = (Block.BLOCK_SIZE*Chunk.CHUNK_SIZE)/(float)Slices;
	float axis = Mathf.Round(Mathf.Sqrt(Slices));
	
	float zS = Mathf.Round(i/axis)/axis;
	float xS = i-Slices*zS;
		
		//Vector3 RandPoint = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f));
		Vector3 RandPoint = Object.transform.TransformPoint(new Vector3(xS,Random.Range(-50,50)/100.0f,zS));
		return RandPoint;
	}

	
	public Vector3 RandomPointInBounds(GameObject Object)
	{
		Vector3 RandPoint = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f));
		RandPoint = Object.transform.TransformPoint(RandPoint * 0.5f);
		return RandPoint;
	}

	
	Vector3 oldRot;
	Vector3 OldPos;
	void LateUpdate()
	{
		Vector3 Latest = cameraT.rotation.eulerAngles;
		Vector3 Dr = Latest-oldRot;
		Vector3 Pos = cameraT.position;
		Vector3 Dp = Pos-OldPos;
		
		if(Mathf.Abs(Dr.y) > 2 || Mathf.Abs(Dr.z) > 2 || Dp.sqrMagnitude > 1)
		{
		oldRot = Latest;
		OldPos = Pos;
		ShootRays();
		
		}
		
	
	}
	
	int i = 0;

	List<Chunk> chunks;
	int j = 0;
	Vector3 offset = new Vector3(0,1,0);
	Vector3 LastPos;
	public  void ShootRays()
	{

	

			float x = 0;
			float z = 0;
			
			float dx = (float)cam.pixelWidth/40.0f;
			float dz = (float)cam.pixelHeight/40.0f;
		
			while(x <= 40)
			{
			
			while(z <= 40)
			{
			
			Ray ray = cam.ScreenPointToRay(new Vector3(dx*x + Random.Range(-dx,dx),dz*z+ Random.Range(-dz,dz),0));
			
			RaycastHit[] result;
			result = Physics.RaycastAll(ray).OrderBy(h=>h.distance).ToArray();
			
			if(result.Count() > 0)
			{
			GameObject o = result[0].collider.gameObject;


				GameObject k = result[0].collider.transform.root.gameObject;
				Chunk other = k.GetComponent<Chunk>();
				if(other != null)
				{
						
						
					other.Visible = true;
					other.HideTime = 60;
					other.UpdateRenderStatus();
				}
			
			}
			z++;
			}
			x++;
			z = 0;
		
		}
//yield return new WaitForSeconds(0.1f);
		j++;
		

			

		
	}

}
