﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using UnityStandardAssets.Characters.FirstPerson;
public class Player : MonoBehaviour {

	Point CPos = new Point(550,473);
	public InventoryWindow InventoryWindow;
	public bool GamePlayPaused = false;
	public GameObject TestNPC;
	public GameObject SmellPartical;
	
	public float FoodInSystem = 1.0f;
	public float FoodInStomach = 1.0f;
	public float WaterInSystem = 1.0f;
	public float WaterInStomach = 1.0f;
	public float Sleep = 1.0f;
	public float Energy = 1.0f;

	public FirstPersonController FPSControl;
	public Camera FPSCamera;
	public CharacterController CharCont;

	public bool WorkingOnTask;
	
	//public Bodypart[] Bodyparts = new Bodypart[6];

	public Bodypart Head;
	public Bodypart Torso;
	public Bodypart Arms;
	public Bodypart Legs;
	
	public Item WeildedItem;
	public Container Inventory;
	public Container Equipment;
	public PlayerDamageTaker PlayerDamageTaker;

	public List<VisionLight> Lights = new List<VisionLight>();



	int CurrentInteraction = 0;
	void Awake()
	{
		Global.Player = this;
	}

	void OnTriggerExit(Collider other)
	{
		VisionLight VLight = other.GetComponent<VisionLight> ();
		if (VLight != null) {
			Lights.Remove(VLight);
		}
		
	}


	void OnTriggerEnter(Collider other)
	{
		VisionLight VLight = other.GetComponent<VisionLight> ();
		if (VLight != null) {
			Lights.Add(VLight);
		}

	}

	public VisionLight CurrentLight()
	{
		VisionLight Brightest = null;
		foreach (VisionLight VLight in Lights) 
		{
			if(Brightest == null || VLight.Brightness > Brightest.Brightness)
			{
				Brightest = VLight;
			}
		}

		return Brightest;
	}

	public float CurrentLightLevel()
	{
		VisionLight VLight = CurrentLight ();

		if (VLight != null) {
			return VLight.Brightness;
		}
		return 0;
	}


	void Start () {
	
	System.Random r = new System.Random();
	
	Vector3 pos = new Vector3(r.Next(2000,2500),5,r.Next(2000,2500));

	
	System.Windows.Forms.Cursor.Position = CPos;
	UnityEngine.Cursor.visible = false;
	UnityEngine.Cursor.lockState = CursorLockMode.Locked;

	System.Windows.Forms.Cursor.Position = CPos;
	
	InvokeRepeating("PlaceSmellPartical",0,1);
	InvokeRepeating("TickNeeds",0,2);
	
	
	
	
	}
	
	public void Die()
	{
		UnityEngine.Application.LoadLevel("Titlescreen");
	}

	public void CheckBleedingInfection(Item.DamageTypes DamageType,float Damage,Bodypart part)
	{
		if (Random.Range (0, 100) < 10) {
			part.Bleeding = true;
		}

		if (Random.Range (0, 100) < 10) {
			part.Infected = true;
		}

	}



	public void ClearCurrentTarget()
	{

		//xczcxCurrentInteraction = 0;
		if (CurrentTarget != null) 
		{
			CurrentTarget.selected = false;
			CurrentTarget = null;
		}
			Global.UIMaster.HUD.SelectionBox.active = false;
			Global.UIMaster.HUD.MouseText.text = "";
			ClearActionLabels();

			
	}

	public Interactable CurrentTarget = null;

	public void UpdateInteractableOptions()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);


		RaycastHit hit = new RaycastHit();

		if (Physics.Raycast (ray, out hit, 20)) {
			Interactable I = hit.collider.GetComponent<Interactable> ();

			if ( I == null || (CurrentTarget != null && I != CurrentTarget)) 
			{
				ClearCurrentTarget ();
			}

			if (I != null) {
				CurrentTarget = I;
				Global.UIMaster.HUD.MouseText.text = I.MouseOverName;
				Global.UIMaster.HUD.SelectionBox.active = true;
				Global.UIMaster.HUD.SelectionBox.transform.localScale = I.GetComponent<Collider> ().bounds.size;
				Global.UIMaster.HUD.SelectionBox.transform.position = I.GetComponent<Collider> ().bounds.center;
				SetHUDActionLabels (I);
			}
			else if(WeildedItem != null)
			{
				SetHUDActionLabelsFromItem(WeildedItem);
			}

		} else 
		{
			ClearCurrentTarget();

			if(WeildedItem != null)
			{
				SetHUDActionLabelsFromItem(WeildedItem);
			}
		}

	}

	void SetHUDActionLabels(Interactable obj)
	{
		int i = 0;
		//CurrentInteraction = 0;
		foreach(Interaction I in obj.Interactions)
		{
			if(i < Global.UIMaster.HUD.ActionLabels.Count)
			{
				Global.UIMaster.HUD.ActionLabels[i].gameObject.transform.parent.gameObject.active = true;
				Global.UIMaster.HUD.ActionLabels[i].Text.text = I.Name;
				i++;
			}
			
		}
		
		if(i < Global.UIMaster.HUD.ActionLabels.Count)
		{
			while(i< Global.UIMaster.HUD.ActionLabels.Count)
			{
				Global.UIMaster.HUD.ActionLabels[i].gameObject.transform.parent.gameObject.active = false;
				i++;
			}
		}
		//Global.UIMaster.HUD.ActionLabels[CurrentInteraction].SetSelected(true);
	}

	void SetHUDActionLabelsFromItem(Item obj)
	{
		int i = 0;
	//	CurrentInteraction = 0;
		foreach(Interaction I in obj.ItemActions)
		{
			if(i < Global.UIMaster.HUD.ActionLabels.Count)
			{
				Global.UIMaster.HUD.ActionLabels[i].gameObject.transform.parent.gameObject.active = true;
				Global.UIMaster.HUD.ActionLabels[i].Text.text = I.Name;
				i++;
			}
			
		}
		
		if(i < Global.UIMaster.HUD.ActionLabels.Count)
		{
			while(i< Global.UIMaster.HUD.ActionLabels.Count)
			{
				Global.UIMaster.HUD.ActionLabels[i].gameObject.transform.parent.gameObject.active = false;
				i++;
			}
		}

		//Global.UIMaster.HUD.ActionLabels[CurrentInteraction].SetSelected(true);
	}

	void ClearActionLabels()
	{
		int i = 0;
		
		while(i< Global.UIMaster.HUD.ActionLabels.Count)
		{
			if(Global.UIMaster.HUD.ActionLabels[i] != null)
			{
			Global.UIMaster.HUD.ActionLabels[i].gameObject.transform.parent.gameObject.active = false;
			}
			i++;
		}
		
		
	}

	public void Attacked(Item.DamageTypes DamageType,float Damage,Bodypart part)
	{

		part.Health -= Damage;
		CheckBleedingInfection(DamageType,Damage,part);
		
		Global.UIMaster.HeadMeter.SetValue ((float)Head.Health / (float)Head.MaxHealth,Head.Infected,Head.Bleeding);
		Global.UIMaster.TorsoMeter.SetValue ((float)Torso.Health / (float)Torso.MaxHealth,Torso.Infected,Torso.Bleeding);
		Global.UIMaster.ArmsMeter.SetValue ((float)Arms.Health / (float)Arms.MaxHealth,Arms.Infected,Arms.Bleeding);
		Global.UIMaster.LegsMeter.SetValue ((float)Legs.Health / (float)Legs.MaxHealth,Legs.Infected,Legs.Bleeding);

		int Bodypart = Random.Range(0,6);
		
		if(PlayerDamageTaker.health <= 0)
		{
			Die();
		}
	
	}
	
	public float GetWeaponSwingTime()
	{
		if (WeildedItem != null) {
			return WeildedItem.AttackSpeed;
		}

		return 1.0f;
	}
	
	
	// integrate this better into interaction system
	
	float SwingRecoverTime = 0;
	void SwingHeldItem()
	{
	
		if(SwingRecoverTime < Time.time)
		{
		
	    Ray ray = FPSCamera.ScreenPointToRay(new Vector3(FPSCamera.pixelWidth/2.0f, FPSCamera.pixelHeight/2.0f, 0));
        //Debug.DrawRay(ray.origin, ray.direction * 10, UnityEngine.Color.yellow,10,true);
		RaycastHit hit;

			float attackrange = 1.0f;

		if(WeildedItem != null)
		{
				attackrange = WeildedItem.AttackRange;
		}

		Physics.Raycast(ray, out hit,attackrange*Block.BLOCK_SIZE);
		
		if(hit.collider != null)
		{
			//GameObject O = hit.collider.transform.root.gameObject;
			DamageTaker N = hit.collider.GetComponentInParent<DamageTaker>();
			NPC NPC = N.GetComponent<NPC>();
			if(N != null)
			{
			
				int BaseDamage = Random.Range(0,10);
			
				if(WeildedItem != null)
				{
					N.TakeDamage(WeildedItem.DamageType,WeildedItem.Damage+BaseDamage,5);
					Global.UIMaster.OutputBox.AddMessage("you hit the " + N.gameObject.name + " for " + (WeildedItem.Damage+BaseDamage) + " damage",TextBoxI.MColor.Green);
				}
				else
				{
					N.TakeDamage(Item.DamageTypes.Smashing,BaseDamage,5);
					Global.UIMaster.OutputBox.AddMessage("you hit the " + N.gameObject.name + " for " + BaseDamage + " damage",TextBoxI.MColor.Green);
				}
				
				
			}
			
			if(NPC != null)
			{
				NPC.LastAttacker = this;
			}
			
		}
		SwingRecoverTime = Time.time + GetWeaponSwingTime();
		}
		
	}

	public float CalculateEncumberance(Item.ClothingSlot Slot)
	{
		
		float encumberance = 0;
		foreach (Item I in Equipment.Contents) 
		{
			if(I.Coverage.Contains(Slot))
			{
				

				encumberance += I.Encumberance;
			}
			
		}
		return encumberance;
	}

	public float CalculateAmour(Item.ClothingSlot Slot,Item.DamageTypes DamageType,bool CoverageMatters)
	{

		float amour = 0;
		foreach (Item I in Equipment.Contents) 
		{
			if(I.Coverage.Contains(Slot))
			{

			foreach(Item.ProtectionType Type in I.Protection)
			{
				if(Type.DamageType == DamageType)
				{
					if(!CoverageMatters || Random.value <= I.ClothingCoverage)
					{
						amour+= Type.Level;
					}
				}
			}

			}

		}
		return amour;
	}



	void TickNeeds()
	{



		Head.TickBleedingAndInfection ();
		Arms.TickBleedingAndInfection ();
		Legs.TickBleedingAndInfection ();
		Torso.TickBleedingAndInfection();

		
		Global.UIMaster.HeadMeter.SetValue ((float)Head.Health / (float)Head.MaxHealth,Head.Infected,Head.Bleeding);
		Global.UIMaster.TorsoMeter.SetValue ((float)Torso.Health / (float)Torso.MaxHealth,Torso.Infected,Torso.Bleeding);
		Global.UIMaster.ArmsMeter.SetValue ((float)Arms.Health / (float)Arms.MaxHealth,Arms.Infected,Arms.Bleeding);
		Global.UIMaster.LegsMeter.SetValue ((float)Legs.Health / (float)Legs.MaxHealth,Legs.Infected,Legs.Bleeding);

		if (Head.Health <= 0 || Torso.Health <= 0 || Legs.Health <= 0 || Arms.Health <= 0 || FoodInSystem <= 0 || WaterInSystem <= 0) {
			Die();
		}

		float DigestedFood = 0.3f*FoodInStomach;
		FoodInStomach-= DigestedFood;
		FoodInSystem+= DigestedFood;
		
		float AbsorbedWater = 0.3f*WaterInStomach;
		WaterInStomach-= AbsorbedWater;
		WaterInSystem+= AbsorbedWater;
		
		FoodInSystem -= 0.002f;
		WaterInSystem -= 0.002f;
		
		
		if(FoodInSystem > 1.0f)
		{
			FoodInSystem = 1.0f;
		}
		
		if(WaterInSystem > 1.0f)
		{
			WaterInSystem = 1.0f;
		}
		
		if(Energy < 1.0f)
		{
		Energy+= 0.1f;
		Sleep-= 0.005f;
		
		FoodInSystem -= 0.002f;
		WaterInSystem -= 0.002f;
		
		}
		Sleep -= 0.002f;
		
		Global.UIMaster.Hunger.SetValue(FoodInSystem);
		Global.UIMaster.Water.SetValue(WaterInSystem);
		Global.UIMaster.Sleep.SetValue(Sleep);
	}
	
	void PlaceSmellPartical()
	{
	GameObject smell = ((GameObject)(GameObject.Instantiate(SmellPartical,transform.position,Quaternion.identity)));
	}
	
	bool first = true;
	bool DisplayInventory = false;
	// Update is called once per frame
	
	void LateUpdate()
	{
		System.Windows.Forms.Cursor.Position = CPos;
	}

	public void BeginTask()
	{
		WorkingOnTask = true;
		FPSControl.MovementEnabled = false;
	}

	public void EndTask()
	{
		WorkingOnTask = false;
		FPSControl.MovementEnabled = true;
	}

	public void AbandonTask()
	{
		if (WorkingOnTask) {
			Global.TimeManager.AbandonTask ();
			Global.UIMaster.OutputBox.AddMessage ("you abandon your task");
			EndTask ();
		}
	}

	void Update ()
	{

		Inventory.UpdateTickItems ();
		Equipment.UpdateTickItems ();


		if (WorkingOnTask) 
		{

			FPSControl.MovementEnabled = false;

			if(Input.GetKeyDown(KeyCode.Escape))
			{
				AbandonTask();
			}

		}


		if (WeildedItem != null && !Inventory.Contents.Contains (WeildedItem) && !Equipment.Contents.Contains (WeildedItem)) 
		{
			WeildedItem = null;
		}

		UpdateInteractableOptions ();

	if(first)
	{
	System.Windows.Forms.Cursor.Position = CPos;
	//first = false;
	}


		if(!WorkingOnTask && Input.GetKeyDown(KeyCode.E) && !Input.GetKey(KeyCode.LeftShift))
		{	
			if(CurrentTarget != null)
			{
				CurrentTarget.Interactions[CurrentInteraction].Interact(CurrentTarget.gameObject,WeildedItem);
			}
			else if(WeildedItem != null && WeildedItem.ItemActions.Count > 0)
			{
				WeildedItem.ItemActions[CurrentInteraction].Interact(null,WeildedItem);
			}
		}


		float wheel = Input.GetAxis("Mouse ScrollWheel");

		if (wheel != 0) 
		{


			int interactioncount = 0;

			if(WeildedItem != null)
			{
				interactioncount = WeildedItem.ItemActions.Count;
			}

			if(CurrentTarget != null)
			{
				interactioncount = CurrentTarget.Interactions.Count;
			}

			if(CurrentInteraction > interactioncount-1)
			{
				CurrentInteraction = 0;
			}


			Global.UIMaster.HUD.ActionLabels[CurrentInteraction].SetSelected(false);
			if (wheel > 0) 
			{
				if (this.CurrentInteraction > 0)
				{
					this.CurrentInteraction--;
				}
			}
			else if (wheel < 0)
			{
				if (this.CurrentInteraction < interactioncount-1) 
				{
					this.CurrentInteraction++;
				}

			}
			Global.UIMaster.HUD.ActionLabels[CurrentInteraction].SetSelected(true);

		}

		if(Input.GetKeyDown(KeyCode.F1))
		{	
				Global.NPCManager.SpawnNPC(0,transform.position);
		}
		
		if(Input.GetKeyDown(KeyCode.F2))
		{	
			GameObject smell = ((GameObject)(GameObject.Instantiate(SmellPartical,transform.position,Quaternion.identity)));
		}
		
		if(Input.GetKeyDown(KeyCode.I))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Normal);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetKeyDown(KeyCode.E) && Input.GetKey(KeyCode.LeftShift))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Eat);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetKeyDown(KeyCode.R) && Input.GetKey(KeyCode.LeftShift))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Read);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetKeyDown(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Weild);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetKeyDown(KeyCode.P) && Input.GetKey(KeyCode.LeftShift))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Wear);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetKeyDown(KeyCode.D) && Input.GetKey(KeyCode.LeftShift))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Drop);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetKeyDown(KeyCode.M))
		{	
			AbandonTask();
			Global.UIMaster.InventoryWindow.ShowInventory(InventoryWindow.InventoryMode.Map);
			DisplayInventory = !DisplayInventory;
		}
		if(Input.GetMouseButtonDown(0))
		{	
			AbandonTask();
			SwingHeldItem();
		}
		

	
	}
}
