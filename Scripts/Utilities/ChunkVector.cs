﻿using UnityEngine;
using System.Collections;

public class ChunkVector
{
	public Vector2 V = Vector2.zero;

	public ChunkVector(int x, int y)
	{
		V = new Vector2(x,y);
	}
}
