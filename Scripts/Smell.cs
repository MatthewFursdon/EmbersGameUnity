﻿using UnityEngine;
using System.Collections;

public class Smell : MonoBehaviour {

	public int LifeTime = 1;
	public float Interest = 0.5f; //how interesting a smell from 0-1
	float startinterest = 0.5f;
	public int TimesDecreased = 0;
	public GameObject toscale;
	// Use this for initialization
	void Start () 
	{
		startinterest = Interest;
		InvokeRepeating("FadeSmell",1,1);
	}
	
	// Update is called once per frame
	void FadeSmell()
	{

			float s = (10.0f - TimesDecreased)/10.0f;
			toscale.transform.localScale = new Vector3(s,s,s);
			Interest = s*startinterest;
		if(Interest <= 0)
		{
			Object.Destroy(transform.parent.gameObject);
		}
		
		TimesDecreased++;
	
	}
}
