﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TimeControl: MonoBehaviour {

	public Text TimeText;

	void Start()
	{
		TimeText = GetComponent<Text> ();
	}


	public void SetTime(System.DateTime Time)
	{
		TimeText.text = Time.ToShortTimeString ();
	}
}
