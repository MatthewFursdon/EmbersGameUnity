﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ItemDescriptionWindow : MonoBehaviour 
{


public Text ItemName;
public Text ItemWeight;
public Text ItemVolume;
public Text ItemDescription;
public Text ItemDamage;
public Text ItemAttackSpeed;

public Text ContentsName;
public Text ContentsDescription;

public void SetItem(Item I)
{
	ItemName.text = I.DisplayName;
	ItemWeight.text = "Weight: " + I.Weight();
	ItemVolume.text = "Volume: " + I.Volume();
	ItemDamage.text = I.Damage + " damage, " + I.DamageType.ToString();
	ItemAttackSpeed.text = I.AttackSpeed + "s swing speed";
	ItemDescription.text = I.Description;

	if (I.ContainerContents != null) 
	{
			ContentsName.text = I.ContainerContents.DisplayName + "("+I.ContainerVolumeFilled+"/"+I.ContainerVolume+")";
			ContentsDescription.text = I.ContainerContents.Description;
	} 
	else 
	{
			ContentsName.text ="";
			ContentsDescription.text = "";
	}

}


}
