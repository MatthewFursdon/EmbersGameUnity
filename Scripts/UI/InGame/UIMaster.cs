﻿using UnityEngine;
using System.Collections;

public class UIMaster : MonoBehaviour {

	public HUD HUD;
	public TextBoxI OutputBox;
	
	public TextMeter Hunger;
	public TextMeter Water;
	public TextMeter Sleep;
	public TextMeter Morale;

	public BarMeter HeadMeter;
	public BarMeter TorsoMeter;
	public BarMeter LegsMeter;
	public BarMeter ArmsMeter;

	public TimeControl Time;
	
	public TextMeter TemporaryHealth;
	
	public GameObject MainUI;
	public InventoryWindow InventoryWindow;
	
	// Use this for initialization
	void Awake () {
	Global.UIMaster = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
