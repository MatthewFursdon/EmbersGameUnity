﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class HUD : MonoBehaviour {

	public Text MouseText;
	public GameObject SelectionBox;
	public List<InteractionLabel> ActionLabels;
}
