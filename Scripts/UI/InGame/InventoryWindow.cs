﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Reflection;
public class InventoryWindow : MonoBehaviour
{
	public enum InventoryMode{Normal,Wear,Weild,Eat,Activate,Drop,TakeOff,Read,Selection,Map}

	public Text WeaponsLabel;
	public Text ToolsLabel;
	public Text FoodDrinkLabel;
	public Text OtherLabel;
	public Text AmmoLabel;
	public Text ClothingLabel;
	public Text BooksLabel;
	public Text Weight;
	public Text Volume;
	public Text WornClothing;
	public Text HeldItem;
	public int position;

	public UnityEngine.UI.Image MapWindow;

	public TextValueMeter HeadEncMeter;
	public TextValueMeter TorsoEncMeter;
	public TextValueMeter ArmsEncMeter;
	public TextValueMeter LegsEncMeter;
	public TextValueMeter FeetEncMeter;
	public TextValueMeter HandsEncMeter;

	public AmourMeter HeadAmourMeter;
	public AmourMeter TorsoAmourMeter;
	public AmourMeter LegsAmourMeter;
	public AmourMeter FeetAmourMeter;
	public AmourMeter HandsAmourMeter;
	public AmourMeter ArmsAmourMeter;




	public GameObject ItemLabelPrefab;
	public GameObject InventoryHeaderLabels;
	public GameObject ClothingHeaderLables;
	public ItemDescriptionWindow ItemDescriptionWindow;
	public InventoryWindow.InventoryMode CurrentMode;
	public List<ItemLabel> ItemLabels = new List<ItemLabel>();
	private int SelectedItem = 1;
	public Item selection = null;



	public MethodInfo CB;
	public System.Object rf;

	public void Start()
	{
		Global.UIMaster.InventoryWindow = this;
		base.InvokeRepeating("Refresh", 1f, 1f);
	}

	public void ShowInventory(InventoryWindow.InventoryMode mode)
	{
		this.CurrentMode = mode;
		Global.UIMaster.InventoryWindow.gameObject.active = (true);
		Refresh ();
		//Global.UIMaster.MainUI.active =(false);
	}

	IEnumerator test()
	{
		while (selection == null) {
			yield return new WaitForSeconds(1);
		}
	}

	public void ItemSelectionScreen(MethodInfo callback,System.Object instance)
	{
		selection = null;
		ShowInventory(InventoryWindow.InventoryMode.Selection);
		CB = callback;
		rf = instance;

	}

	public void HideInventory()
	{
		Global.UIMaster.InventoryWindow.gameObject.active = (false);
		Global.UIMaster.MainUI.active = (true);
	}

	public ItemLabel GenerateLabel(Item I, float y, GameObject O)
	{
		if (I != null) {
			ItemLabel component = ((GameObject)GameObject.Instantiate (this.ItemLabelPrefab, new Vector3 (1f, 1f, 1f), Quaternion.identity)).GetComponent<ItemLabel> ();
			component.Text.text = (I.GenerateFullName ());
		

		
			component.transform.parent = O.transform;
			//component.transform.localPosition =(new Vector3(-268.6f, y, 0f));
			component.transform.localPosition = (new Vector3 (0, y, 0f));
			component.Item = I;
			return component;
		}
		return null;
	}

	public void HideAllCategorys()
	{
		WeaponsLabel.enabled = (false);
		ToolsLabel.enabled = (false);
		FoodDrinkLabel.enabled = (false);	
		OtherLabel.enabled = (false);
		AmmoLabel.enabled = (false);
		BooksLabel.enabled = (false);
	}

	public void RefreshCategory(List<Item> items, Text header, ref int t)
	{
		if (items.Count > 0)
		{
			//header.transform.localPosition =(new Vector3(-274.5f, (float)t, 0f));
			header.transform.localPosition =(new Vector3(0, (float)t, 0f));
			header.enabled = (true);
			t -= 20;
			
			foreach(Item I in items)
			{
					ItemLabel itemLabel = this.GenerateLabel(I, (float)t, header.gameObject);
					itemLabel.SetSelected(false);
					this.ItemLabels.Add(itemLabel);
					t -= 20;
			}

		}
		else
		{
			header.enabled =(false);
		}
		t -= 15;
	}

	public void RefreshWornClothing()
	{
			int j = 0;
			foreach(Item I in Global.Player.Equipment.Contents)
			{
			
			ItemLabel itemLabel = this.GenerateLabel(I, (float)j, ClothingHeaderLables);
			itemLabel.SetSelected(false);
			this.ItemLabels.Add(itemLabel);
			j -= 20;
			}
	}
	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Return))
		{
			if (this.CurrentMode == InventoryWindow.InventoryMode.Normal)
			{
				this.ItemDescriptionWindow.SetItem(this.ItemLabels[this.SelectedItem].Item);
				this.ItemDescriptionWindow.gameObject.SetActive(true);
			}
			else if (this.CurrentMode == InventoryWindow.InventoryMode.Eat)
			{
				if (ItemLabels[this.SelectedItem].Item.ContainerVolume == 0) 
				{
					if(this.ItemLabels[this.SelectedItem].Item.EatItem())
					{
						Global.Player.Inventory.RemoveItem(this.ItemLabels[this.SelectedItem].Item);
						Refresh();
					}
				}
				else
				{
					if(ItemLabels[this.SelectedItem].Item.ContainerContents != null)
					{

						if(ItemLabels[this.SelectedItem].Item.ContainerContents.EatItem())
						{
							Global.Player.Inventory.ReduceItemContent(this.ItemLabels[this.SelectedItem].Item);
							Refresh();
						}

					}
				}
				//this.HideInventory();
			}
			else if (this.CurrentMode == InventoryWindow.InventoryMode.Weild)
			{
				if(Global.Player.WeildedItem != null)
				{
					Global.Player.WeildedItem.StopWielding();
				}

				Global.Player.WeildedItem = this.ItemLabels[this.SelectedItem].Item;
				this.HideInventory();

				Global.Player.WeildedItem.StartWielding();
			}
			else if (this.CurrentMode == InventoryWindow.InventoryMode.Wear)
			{
				Global.Player.Inventory.MoveItemBetweenContainers(Global.Player.Equipment,this.ItemLabels[this.SelectedItem].Item);
				Refresh();
				//Global.Player.WeildedItem = this.ItemLabels[this.SelectedItem].Item;
				//this.HideInventory();
			}
			else if (this.CurrentMode == InventoryWindow.InventoryMode.Drop)
			{
				if(Global.Player.Inventory.RemoveItem(ItemLabels[this.SelectedItem].Item))
				{
				Item.SpawnItem(ItemLabels[this.SelectedItem].Item,Global.Player.transform.position,Quaternion.identity);
				Refresh();
				}
				//Global.Player.WeildedItem = this.ItemLabels[this.SelectedItem].Item;
				//this.HideInventory();
			}
			else if (this.CurrentMode == InventoryWindow.InventoryMode.Selection)
			{
				selection = ItemLabels[this.SelectedItem].Item;
				HideInventory();
				CB.Invoke(rf,null);
				CB = null;
				rf = null;

			}

		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if (this.ItemDescriptionWindow.gameObject.active)
			{
				this.ItemDescriptionWindow.gameObject.SetActive(false);
			}
			else
			{
				this.HideInventory();
			}
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			if (this.SelectedItem >= 0 && this.SelectedItem < this.ItemLabels.Count)
			{
				this.ItemLabels[this.SelectedItem].SetSelected(false);
			}
			this.SelectedItem--;
			if (this.SelectedItem >= 0 && this.SelectedItem < this.ItemLabels.Count)
			{
				this.ItemLabels[this.SelectedItem].SetSelected(true);
			}
			if (this.SelectedItem < 0)
			{
				this.SelectedItem = this.ItemLabels.Count;
			}
			if (this.SelectedItem >= this.ItemLabels.Count)
			{
				this.SelectedItem = 0;
			}
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			if (this.SelectedItem >= 0 && this.SelectedItem < this.ItemLabels.Count)
			{
				this.ItemLabels[this.SelectedItem].SetSelected(false);
			}
			this.SelectedItem++;
			if (this.SelectedItem >= 0 && this.SelectedItem < this.ItemLabels.Count)
			{
				this.ItemLabels[this.SelectedItem].SetSelected(true);
			}
			if (this.SelectedItem < 0)
			{
				this.SelectedItem = this.ItemLabels.Count - 1;
			}
			if (this.SelectedItem >= this.ItemLabels.Count)
			{
				this.SelectedItem = 0;
			}
		}
	}

	public void RefreshEncumberanceAndAmour()
	{
		HeadEncMeter.SetValue (Global.Player.CalculateEncumberance (Item.ClothingSlot.Head));
		TorsoEncMeter.SetValue (Global.Player.CalculateEncumberance (Item.ClothingSlot.Torso));
		ArmsEncMeter.SetValue (Global.Player.CalculateEncumberance (Item.ClothingSlot.Arms));
		LegsEncMeter.SetValue (Global.Player.CalculateEncumberance (Item.ClothingSlot.Legs));
		FeetEncMeter.SetValue (Global.Player.CalculateEncumberance (Item.ClothingSlot.Feet));
		HandsEncMeter.SetValue (Global.Player.CalculateEncumberance (Item.ClothingSlot.Hands));

		HeadAmourMeter.SetValue(Global.Player.CalculateAmour(Item.ClothingSlot.Head,Item.DamageTypes.Peircing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Head,Item.DamageTypes.Smashing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Head,Item.DamageTypes.Stabbing,false));

		TorsoAmourMeter.SetValue(Global.Player.CalculateAmour(Item.ClothingSlot.Torso,Item.DamageTypes.Peircing,false),
		                         Global.Player.CalculateAmour(Item.ClothingSlot.Torso,Item.DamageTypes.Smashing,false),
		                         Global.Player.CalculateAmour(Item.ClothingSlot.Torso,Item.DamageTypes.Stabbing,false));

		ArmsAmourMeter.SetValue(Global.Player.CalculateAmour(Item.ClothingSlot.Arms,Item.DamageTypes.Peircing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Arms,Item.DamageTypes.Smashing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Arms,Item.DamageTypes.Stabbing,false));

		LegsAmourMeter.SetValue(Global.Player.CalculateAmour(Item.ClothingSlot.Legs,Item.DamageTypes.Peircing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Legs,Item.DamageTypes.Smashing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Legs,Item.DamageTypes.Stabbing,false));

		FeetAmourMeter.SetValue(Global.Player.CalculateAmour(Item.ClothingSlot.Feet,Item.DamageTypes.Peircing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Feet,Item.DamageTypes.Smashing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Feet,Item.DamageTypes.Stabbing,false));

		HandsAmourMeter.SetValue(Global.Player.CalculateAmour(Item.ClothingSlot.Hands,Item.DamageTypes.Peircing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Hands,Item.DamageTypes.Smashing,false),
		                        Global.Player.CalculateAmour(Item.ClothingSlot.Hands,Item.DamageTypes.Stabbing,false));

	}

	public void Refresh()
	{

		
		foreach(ItemLabel I in ItemLabels)
		{
				if (I != null)
				{
					GameObject.Destroy(I.gameObject);
				}
		}

		MapWindow.enabled = false;
		
		this.ItemLabels = new List<ItemLabel>();

		Volume.text = "Weight: " + (Global.Player.Inventory.SumWeight()+Global.Player.Equipment.SumWeight()) + "/" + Global.Player.Inventory.MaxWeight();
		Volume.text = "Volume: " + Global.Player.Inventory.SumVolume() + "/" + Global.Player.Inventory.MaxVolume();
		
		List<Item> Weapons = Global.Player.Inventory.GetItemsWithTag(Item.Tag.Weapon); 
		List<Item> Tools = Global.Player.Inventory.GetItemsWithTag(Item.Tag.Tool); 
		List<Item> FoodDrink = Global.Player.Inventory.GetItemsWithTag(Item.Tag.FoodDrink); 
		List<Item> Other = Global.Player.Inventory.GetItemsWithTag(Item.Tag.Other); 
		List<Item> Ammo = Global.Player.Inventory.GetItemsWithTag(Item.Tag.Ammo); 
		List<Item> Clothing = Global.Player.Inventory.GetItemsWithTag(Item.Tag.Clothing); 
		List<Item> Books = Global.Player.Inventory.GetItemsWithTag(Item.Tag.Book); 
		
		int y = 80;

		ClothingLabel.enabled = false;
		FoodDrinkLabel.enabled = false;
		BooksLabel.enabled = false;
		ToolsLabel.enabled = false;
		OtherLabel.enabled = false;
		AmmoLabel.enabled = false;

		if(CurrentMode == InventoryMode.Wear)
		{
			RefreshCategory(Clothing,ClothingLabel,ref y);
		}
		
		else if(CurrentMode == InventoryMode.Eat)
		{
			RefreshCategory(FoodDrink,FoodDrinkLabel,ref y);
		}
		
		if(CurrentMode == InventoryMode.Read)
		{
			RefreshCategory(Books,BooksLabel,ref y);
		}
		if(CurrentMode == InventoryMode.Selection || CurrentMode == InventoryMode.Normal || CurrentMode == InventoryMode.Drop || CurrentMode == InventoryMode.Weild)
		{
			RefreshCategory(FoodDrink,FoodDrinkLabel,ref y);
			RefreshCategory(Weapons,WeaponsLabel,ref y);
			RefreshCategory(Clothing,ClothingLabel,ref y);
			RefreshCategory(Tools,ToolsLabel,ref y);
			RefreshCategory(Books,BooksLabel,ref y);
			RefreshCategory(Ammo,AmmoLabel,ref y);
			RefreshCategory(Other,OtherLabel,ref y);

		}
		if (CurrentMode == InventoryMode.Map) {
			MapWindow.enabled = true;
		}

		
		if(SelectedItem >= 0 && SelectedItem < ItemLabels.Count)
		{
			ItemLabels[SelectedItem].SetSelected(true);
		}
		//ItemLabels[1].SetSelected(true);
		
		RefreshWornClothing();
		RefreshHeldItem ();
		RefreshEncumberanceAndAmour ();
	}

	public void RefreshHeldItem()
	{
		if (Global.Player.WeildedItem != null) {
			HeldItem.text = "Held Item: " + Global.Player.WeildedItem.GenerateFullName ();
		} else {
			HeldItem.text = "Held Item: none";
		}
	}
}
