﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TextBoxI : MonoBehaviour {
	string[] colors = new string[]{"#ff0000ff","#00ff00ff","#ffff00ff","#fffffff"};
	public enum MColor {Red = 0,Green = 1,Yellow = 2,White = 3};
	public Text TextB;
	// Use this for initialization
	void Start () {
		AddMessage("Startup",MColor.Yellow,false);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//AddMessage("dasd",MColor.Yellow,false);
	}

	public void AddMessage(string Msg,MColor col=MColor.White,bool bold=false)
	{

		if(!bold)
		{
			TextB.text = TextB.text + string.Format("<color=\"{0}\">{1}</color>\n",colors[(int)col],Msg);
		}
		else
		{
			TextB.text = TextB.text + string.Format("<b><color=\"{0}\">{1}</color></b>\n",colors[(int)col],Msg);
		}
		
		
		int size = 1300;
		if(TextB.text.Length > size)
		{
			int end = TextB.text.IndexOf("\n",TextB.text.Length-20);
			int start = TextB.text.IndexOf("\n",TextB.text.Length-size-200);
			TextB.text = TextB.text.Substring(start,end - start+1);
		}
	}
}
