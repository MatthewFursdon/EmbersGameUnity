﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextValueMeter : MonoBehaviour {
	
	public float CurrentValue;

	public Color[] LabelColors;
	public float[] LabelCutoffs;
	
	public Text Text;
	
	public void SetValue(float value)
	{
		CurrentValue = value;
		int i = 0;
		while(i < LabelCutoffs.Length)
		{
			
			if(value <= LabelCutoffs[i])
			{
				i++;
			}
			else
			{
				break;
			}
			
			
			
			
		}
		if(i > 0)
		{
			i--;
		}
		
		Text.text = CurrentValue.ToString();
		LabelColors[i].a = 255;
		Text.color = LabelColors[i];
		
		
	}
	
}
