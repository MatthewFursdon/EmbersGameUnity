﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ItemLabel : MonoBehaviour {

	public Text Text;
	public Item Item;
	public bool Selected;
	public Image SelectedBackground;
	
	public void SetSelected(bool selected)
	{
		Selected = selected;
		
		if(Selected)
		{
			SelectedBackground.enabled = true;
		}
		else
		{
			SelectedBackground.enabled = false;
		}
	
	}

}
