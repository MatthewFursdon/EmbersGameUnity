﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Compass : MonoBehaviour {

	public Text Text;
	public Quaternion north;
	// Use this for initialization

	int North = 0;
	int East = 90;
	int South = 180;
	int West = 270;

	string CompassString = "...N...E...S...W";

	string GetSubstring(int charac)
	{
		int sidelength = 4;
		string S = "";
		int start = charac - sidelength;
		int end = charac + sidelength;

		if (start < 0) {

			int leftover = System.Math.Abs (start);
			S += CompassString.Substring (CompassString.Length - leftover-1);
			S += CompassString.Substring (0, leftover+1);

		} else {
			S += CompassString.Substring (start, sidelength);
		}

		S+= CompassString[charac];

		if (end > CompassString.Length) {
			int leftover = end - CompassString.Length;
			S += CompassString.Substring (charac+1);
			S += CompassString.Substring (0,leftover+1);
		} else {
			S += CompassString.Substring (charac+1, sidelength);
		}
		return S;
	}

	void Start () {
		Text = this.GetComponent<Text> ();	
		north = Quaternion.Euler (new Vector3 (0, 0, 0));
	}


	// Update is called once per frame
	void Update () {



		Vector3 angle = Camera.main.transform.rotation.eulerAngles;

		int bucket = (int)Mathf.Round (angle.y / 22.5f)+3;

		if (bucket > CompassString.Length) {
			bucket = CompassString.Length;
		}

		Text.text = GetSubstring (bucket);

	}
}
