﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextMeter : MonoBehaviour {

	public float CurrentValue;
	
	public string[] Labels;
	public Color[] LabelColors;
	public float[] LabelCutoffs;
	
	public Text Text;
	
	public void SetValue(float value)
	{
	CurrentValue = value;
		int i = 0;
		while(i < LabelCutoffs.Length)
		{
			
			if(value <= LabelCutoffs[i])
			{
				i++;
			}
			else
			{
				break;
			}
			
			
			
		
		}
			if(i > 0)
			{
			i--;
			}
			
			Text.text = Labels[i];
			LabelColors[i].a = 255;
			Text.color = LabelColors[i];
			
	
	}
	
}
