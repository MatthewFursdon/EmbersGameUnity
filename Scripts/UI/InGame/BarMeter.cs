﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class BarMeter : MonoBehaviour {


	public Text BarText;
	public Text Label;
	public int MaxBars = 10;
	public int bars = 10;

	public Color InfectedColor;
	public Color BleedingColor;
	public Color NormalColor;
	// Use this for initialization
	void Start () 
	{
		BarText = GetComponent<Text> ();
	}

	public void SetValue(float value,bool infected,bool bleeding)
	{
		if (bleeding) {
			BarText.color = BleedingColor;
			Label.color = BleedingColor;

		} else if (infected) {
			BarText.color = InfectedColor;
			Label.color = InfectedColor;
		} else {
			BarText.color = NormalColor;
			Label.color = NormalColor;
		}

		bars = (int)Mathf.Round(MaxBars * value);
		if (bars > 0) {
			BarText.text = new string ('|', bars);
		} else {
			BarText.text = "";
		}
		
	}
}
