﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractionLabel : MonoBehaviour {

	public Text Text;
	bool selected;
	public Color Selected;
	public Color NotSelected;
	public Image BImage;

	public void SetSelected(bool s)
	{
		selected = s;

		if (selected) {
			BImage.color = Selected;
		} else {
			BImage.color = NotSelected;
		}

	}

}
