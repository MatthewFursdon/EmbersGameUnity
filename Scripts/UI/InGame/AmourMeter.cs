﻿using UnityEngine;
using System.Collections;

public class AmourMeter : MonoBehaviour {

	public UnityEngine.UI.Text Text;


	void Start()
	{
		Text = GetComponent<UnityEngine.UI.Text> ();
	}

	public void SetValue(float stab,float pierce,float slash)
	{

		Text.text = stab.ToString () + "/" + pierce.ToString () + "/" + slash.ToString ();
	}
}
