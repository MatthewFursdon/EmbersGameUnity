﻿using UnityEngine;
using System.Collections;
using System.Drawing;
public class MapDisplay : MonoBehaviour {


	public UnityEngine.UI.Image UImage;
	public UnityEngine.UI.Image Player;
	// Use this for initialization
	void Start () {
	
		// load map image

		WWW load = new WWW ("file://test.png");
		Texture2D MapTex = new Texture2D (2,2);
		load.LoadImageIntoTexture (MapTex);
		UImage.sprite = Sprite.Create (MapTex, new Rect (new Vector2 (0, 0), new Vector2 (MapTex.width, MapTex.height)), new Vector2 (0.5f, 0.5f),100.0f,0,SpriteMeshType.FullRect);
		//UImage.material.mainTexture = MapTex;

		UImage.rectTransform.sizeDelta = new Vector2(MapTex.width,MapTex.height);
	}

	float divider = (float)GenerateWorld.WorldSize * Chunk.CHUNK_SIZE * Block.BLOCK_SIZE;

	void Update()
	{
		Vector3 PlayerPos = Global.Player.transform.position;



		float MarkerX = PlayerPos.x / divider;
		float MarkerZ = PlayerPos.z / divider;

		int a = 0;

		Player.rectTransform.anchoredPosition = new Vector2 (MarkerX*UImage.rectTransform.rect.width,MarkerZ*UImage.rectTransform.rect.height);
	}

}
