﻿using UnityEngine;
using System.Collections;

public class ButtonEvents : MonoBehaviour {

	public GenerateWorld WorldGenerate;

	public void ButtonPressPlay()
	{
		Application.LoadLevel("game");
	
	}

	public void ButtonPressGenerate()
	{
		WorldGenerate.GenerateNewWorld();

	}
	// Use this for initialization
	void Start () {

			UnityEngine.Cursor.lockState = CursorLockMode.None;
		UnityEngine.Cursor.visible = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
