﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldMap : MonoBehaviour {

	public PointOfInterest[,] World;
	public PointOfInterest Wilderness;
	public GameObject POI;
	int ViewSizeX = 20;
	public Canvas PParent;
	RectTransform RectT;
	public GridLayoutGroup Grid;

	public PointOfInterest GetPointOfInterest(int x,int z)
	{
		if(World[x,z] != null)
		{
			return World[x,z];
		}
		return Wilderness;

	}
	// Use this for initialization
	void Start () 
	{
		int vx = 0;
		int vz = 0;


		RectT = PParent.GetComponent<RectTransform> ();
		float Dx = RectT.rect.width / (float)ViewSizeX;

		float ViewSizeZ = RectT.rect.height / Dx;
	
		//Grid.cellSize = new Vector2 (RectT.rect.width / (float)ViewSize, RectT.rect.height / (float)ViewSize);

		while (vx < ViewSizeX) 
		{

			while(vz < ViewSizeZ)
			{
				GameObject newPoi = (GameObject)GameObject.Instantiate(POI,Vector3.zero,Quaternion.identity);
				newPoi.GetComponent<RectTransform>().parent = RectT;
				newPoi.GetComponent<RectTransform>().localPosition = new Vector3(Dx*vx,Dx*vz,0);
				newPoi.GetComponent<RectTransform>().localScale = new Vector3(Dx,Dx,1);

				vz++;
			}
			vx++;
			vz = 0;
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
