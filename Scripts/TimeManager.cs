using UnityEngine;
using System.Collections;
using System.Reflection;


public class TimeManager : MonoBehaviour {

	System.DateTime Time;
	System.DateTime Nightfall;
	public float TimeSpeed = 1.0f;
	System.Nullable<System.DateTime> WaitingFor = null;
	public TOD_Time TODTime;
	public TOD_Sky TODSky;
	MethodInfo CallBackMethod;
	System.Object ToCallBack;

	public bool IsSunUp;

	public enum WaitFailReason{NoLight,TooTired,TooSad,NoFaliure};

	// Use this for initialization
	void Start () {
	
		Global.TimeManager = this;
		Time = new System.DateTime      (1990, 1, 1, 8, 55, 0, System.DateTimeKind.Unspecified);
		TODTime.SetTime (Time);
		this.InvokeRepeating ("ticktime", 0, 0.1f);

		TODTime.OnSunrise += sunrise;
		TODTime.OnSunset += sunset;

	}

	public void sunrise()
	{
		IsSunUp = true;
	}

	public void sunset()
	{
		IsSunUp = false;
	}


	void ticktime()
	{
		Time = Time.AddSeconds (TimeSpeed);
		TODTime.AddSeconds(TimeSpeed);
		if (WaitingFor != null && Time > WaitingFor)
		{
			CallBackMethod.Invoke(ToCallBack,null);
			CallBackMethod = null;
			ToCallBack = null;
			WaitingFor = null;
			TimeSpeed = 1.0f;
		}

		Global.UIMaster.Time.SetTime (Time);
		//Global.DayNightManager.TickTime (new System.TimeSpan (0, 0, (int)Mathf.Round(10.0f * TimeSpeed)));


		//	Global.DayNightManager.SetSkyState(Global.DayNightManager.Night,new System.TimeSpan(0,10,0));
		//}

	}

	public WaitFailReason WaitForTime(MethodInfo callback,System.Object instance,System.TimeSpan WaitFor, float timespeed,bool NeedLight)
	{
		if(!NeedLight || (NeedLight && (IsSunUp || Global.Player.CurrentLightLevel() > 0.3))) 
		{
			WaitingFor = Time.Add (WaitFor);
			TimeSpeed = timespeed;
			CallBackMethod = callback;
			ToCallBack = instance;
			return WaitFailReason.NoFaliure;
		}
		else
		{
			return WaitFailReason.NoLight;
		}


	}

	public void AbandonTask()
	{
		CallBackMethod = null;
		ToCallBack = null;
		WaitingFor = null;
		TimeSpeed = 1.0f;
	}

	// Update is called once per frame
	void Update () {
	

	
	}
}
