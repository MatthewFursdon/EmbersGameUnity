﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public class ItemSpawnList : ScriptableObject
{
	public Item[] Items;
	public int SpawnChance = 100;
	public static ItemSpawnList[] SpawnLists;
}
