﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenuAttribute]
public class FlashLight : Item
{
	public GameObject FlashLightTemplate;
	public GameObject Instance;

	public float DrainRate;

	public override Item CloneItem ()
	{

		FlashLight In = (FlashLight)base.CloneItem ();

		In.FlashLightTemplate = FlashLightTemplate;

		return (Item)In;

	}

	public override void StartWielding()
	{
		//Activated = false;
	}

	public bool HasPower()
	{
		return(Charge1Quantity > 0);
	}

	public override void StopWielding()
	{

		if(Instance != null && !Global.Player.Equipment.Contents.Contains(this))
		{
			Global.UIMaster.OutputBox.AddMessage("you turn off the " + DisplayName + " and put it away");
			Activated = false;
			GameObject.Destroy(Instance);
		}
	}

	public void DrainBatteries()
	{
		if (Charge1Quantity > 0) {
			Charge1.LifeTime -= DrainRate;

			Debug.Log(Charge1.LifeTime);
			if(Charge1.LifeTime <= 0)
			{
				Charge1Quantity--;
				Charge1.LifeTime = 1.0f;
			}

		}
	}

	public override void UpdateItem()
	{
		if (Activated) {


			if(!HasPower())
			{
				Activated = false;
			}
			else
			{
				DrainBatteries();
			}

			if (Instance == null) {
				Instance = (GameObject)GameObject.Instantiate (FlashLightTemplate, Vector3.zero, Quaternion.identity);
				Instance.transform.parent = Camera.main.transform;
				Instance.transform.localPosition = new Vector3(0,0,0.2f);
				Instance.transform.localRotation = Quaternion.identity;

				VisionLight L = Instance.GetComponent<VisionLight>();

				if(L != null)
				{
					Global.Player.Lights.Add(L);
				}
			}
		} else {
			if(Instance != null)
			{

				VisionLight L = Instance.GetComponent<VisionLight>();
				
				if(L != null)
				{
					Global.Player.Lights.Remove(L);
				}

				GameObject.Destroy(Instance);
			}
		}
		
	}


}