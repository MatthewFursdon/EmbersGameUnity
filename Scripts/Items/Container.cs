﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Container : MonoBehaviour
 {
	public List<Item> Contents = new List<Item>();
	public float int_MaxWeight;
	public float int_MaxVolume;

	public void UpdateTickItems()
	{
		foreach (Item I in Contents) {
			I.UpdateItem();
		}

	}

	public string GenerateSaveInfo()
	{


		foreach (Item I in Contents) {

			if(I.BaseLine)
			{
				// write baseline identifier
				I.GenerateBaselineSaveInfo();
			}
			else
			{
				I.GenerateSaveInfo();
			}

		}
		return "";
	}

	public float MaxWeight()
	{
		return int_MaxWeight;
	}

	public float MaxVolume()
	{
		float ExtraVolume = 0;
		if (this == Global.Player.Inventory) {

			foreach(Item I in Global.Player.Equipment.Contents)
			{
				ExtraVolume+= I.StorageVolume;
			}

		}

		return int_MaxVolume + ExtraVolume;
	}
	
	
		public List<Item> GetItemsWithAttribute(Item.action Action,int level)
	{
	
		List<Item> outlist = new List<Item>();
		foreach(Item I in Contents)
		{
			foreach(Item.Attribute A in I.Attributes)
			{
				if(A.Action == Action && A.Level >= level)
				{
					outlist.Add(I);
				}
			}
		}
		
		return outlist;
	
	}
	
	public List<Item> GetItemsWithTag(Item.Tag Tag)
	{
	
		List<Item> outlist = new List<Item>();
		foreach(Item I in Contents)
		{
			if(I.Tags.Contains(Tag) || (I.ContainerContents != null && I.ContainerContents.Tags.Contains(Tag)) )
			{
				outlist.Add(I);
			}
		}
		
		return outlist;
	
	}
	
	public bool ReduceItemContent(Item Item)
	{
		if (Item.ContainerVolume > 0 && Item.ContainerContents != null) 
		{
			Item.ContainerVolumeFilled--;
			if(Item.ContainerVolumeFilled <= 0)
			{
				Item.ContainerVolumeFilled = 0;
				Item.ContainerContents = null;

			}
			return true;
		}
		return false;
	}

	public bool ContainsItem(Item item)
	{
		return Contents.Contains(item);
	}
	
	public bool RemoveItem(Item item)
	{
		if(ContainsItem(item))
		{
			Contents.Remove(item);
			return true;
		}
		return false;
	}
	
	public bool MoveItemBetweenContainers(Container destination,Item item)
	{
		if(destination.CanAddItem(item))
		{
			if(destination.AddItem(item))
			{
				RemoveItem(item);
				return true;
			}
		}
		return false;
	
	}
	
	public bool AddItem(Item Item)
	{
		if(CanAddItem(Item))
		{
			Contents.Add(Item);
			return true;
		}
		return false;
	}
	
	public bool CanAddItem(Item Item)
	{
		float weight = SumWeight();
		float volume = SumVolume();
		
		if(volume+Item.Volume() < MaxVolume() && weight+Item.Weight() < MaxWeight())
		{
			return true;
		}
		
		return false;
	}
	
	public float SumWeight()
	{	
		float sum = 0;
		foreach(Item I in Contents)
		{
			if(I != null)
			{
			sum += I.Weight();
			}
		}
		return sum;
	}
	
	public float SumVolume()
	{
		float sum = 0;
		foreach(Item I in Contents)
		{
			if(I != null)
			{
				sum += I.Volume();
			}
		}
		return sum;
	
	}
	
	
}
