﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[CreateAssetMenuAttribute]
public class Item : ScriptableObject	
{
	public static GameObject DefaultWorldObject;
	public GameObject WorldObject;

	[System.Serializable]
	public class Attribute
	{
		public action Action;
		[Range(0, 20)]
		public int Level;
		
		public Attribute(action a,int l)
		{
			Action = a;
			Level = l;
		}
		
	};



		[System.Serializable]
	public class ProtectionType
	{
		public DamageTypes DamageType;
		[Range(0, 20)]
		public int Level;
		
		public ProtectionType(DamageTypes a,int l)
		{
			DamageType = a;
			Level = l;
		}
		
	};

	public float Volume()
	{
		return volume;
	}

	public float Weight()
	{
		if (ContainerContents == null) {
			return weight;
		} else {
			return (ContainerContents.weight*(float)ContainerVolumeFilled)+weight;
		}
	}

	public override bool Equals(object A)
	{

		if (A == null) {
			return false;
		}

		Item Other = A as Item;
		if (Other != null) {
			return (this.InternalName == Other.InternalName);
		}
		return false;
	}

	/*
	public static bool operator ==(Item A,Item B)
	{
		if (A == null || B == null) {
			return false;
		}

		return A.Equals(B);
	}

	public static bool operator !=(Item A,Item B)
	{


		return !A.Equals(B);
	}
	*/

	public override int GetHashCode()
	{
		return InternalName.GetHashCode ();
	}

	public bool BaseLine = true;

	public string DisplayName;
	public string InternalName = "CHANGEME";
	public string Description;
	public float volume;
	public float weight;
	public bool Activated = false;
	
	public enum Tag{Weapon,RangedWeapon,Tool,FoodDrink,Ammo,Other,Clothing,Container,Charges1,Charges2,Charges3,Plastic,Metal,Glass,NeedsContainer,Book,FluidContainer}
	public enum action{Cutting,Butcturing,Digging,Screwing,Hammering,Metal_Sawing,Wood_Sawing,Filing,Containing,Cooking}
	public enum ClothingSlot{Head,Neck,Arms,Hands,Torso,Legs,Feet,Ears,Nose,Fingers,Toes,Eyes};
	public enum DamageTypes{Stabbing,Smashing,Peircing}
	
	public DamageTypes DamageType = DamageTypes.Smashing;
	public float Damage = 0;
	public float HitModifier = 0;
	public float AttackSpeed = 1.0f;
	public float AttackRange = 0.5f; //in tiles

	public Item Charge1;
	public int Charge1Quantity = 0;
	public int Charge1MaxQuantity = 0;
	
	public Item Charge2;
	public int Charge2Quantity = 0;
	public int Charge2MaxQuantity = 0;
	
	public Item Charge3;
	public int Charge3Quantity = 0;
	public int Charge3MaxQuantity = 0;

	public float LifeTime = 1.0f; //eg charge in batterie, light from a glowstick

	public int StorageVolume = 0;

	public int ContainerVolume = 0;
	public int ContainerVolumeFilled;
	public Item ContainerContents;
	public bool Fireproof;
	
	[Range(0,20)]
	public float Encumberance;
	
	[Range(0.0f, 1.0f)]
	public float ClothingCoverage;
	
	public List<Tag> Tags = new List<Tag>();
	public List<Attribute> Attributes = new List<Attribute>();
	public List<ProtectionType> Protection = new List<ProtectionType>();
	public List<ClothingSlot> Coverage = new List<ClothingSlot>();

	public float Quench = 0;
	public float Food = 0;

	public List<Interaction> ItemActions;

	public string GenerateContainerName()
	{
		if (ContainerVolume > 0 && ContainerContents != null) {
			return (DisplayName + " of " + ContainerContents.DisplayName);
		}
		return DisplayName;
	}

	public string GenerateFullName()
	{


		string working = (DisplayName);
		
		if(ContainerVolume > 0 && ContainerContents != null )
		{
			working += "(" + ContainerContents.DisplayName + " " + ContainerVolumeFilled + "/" + ContainerVolume + ")";
		}
		
		if(Charge1 != null)
		{
			working += "(" + Charge1.DisplayName + " " + Charge1Quantity + "/" + Charge1MaxQuantity + ")";
		}
		
		if(Charge2 != null)
		{
			working += "(" + Charge2.DisplayName + " " + Charge2Quantity + "/" + Charge2MaxQuantity + ")";
		}
		
		if(Charge3 != null)
		{
			working += "(" + Charge3.DisplayName + " " + Charge2Quantity + "/" + Charge2MaxQuantity + ")";
		}

		return working;

	}

	public virtual void UpdateItem()
	{

	}

	public virtual void StartWielding()
	{

	}

	public virtual void StopWielding()
	{

	}

	public string GenerateBaselineSaveInfo()
	{
		return "";
		
	}


	public string GenerateSaveInfo()
	{
		return "";

	}
		
	public virtual Item CloneItem()
	{
		Item In = (Item)ScriptableObject.CreateInstance(this.GetType());
		In.DisplayName = DisplayName;
		In.InternalName = InternalName;
		In.Description = Description;
		In.volume = volume;
		In.weight = weight;
		In.Activated = false;
		In.DamageType = DamageType;
		In.Damage = Damage;
		In.HitModifier = HitModifier;
		In.AttackSpeed = AttackSpeed;
		In.LifeTime = LifeTime;
		
		In.Quench = Quench;
		In.Food = Food;
		
		In.Charge1Quantity = Charge1Quantity;
		In.Charge2Quantity = Charge2Quantity;
		In.Charge3Quantity = Charge3Quantity;
		
		In.Charge1MaxQuantity = Charge1MaxQuantity;
		In.Charge2MaxQuantity = Charge2MaxQuantity;
		In.Charge3MaxQuantity = Charge3MaxQuantity;
		
		In.LifeTime = 1.0f;
		
		if (Charge1 != null)
		{
			In.Charge1 = Charge1.CloneItem ();
		}
		
		if (Charge2 != null) 
		{
			In.Charge2 = Charge2.CloneItem ();
		}
		
		if (Charge3 != null) 
		{
			In.Charge3 = Charge3.CloneItem ();
		}

		In.StorageVolume = StorageVolume;

		In.ContainerVolume = ContainerVolume;
		In.ContainerVolumeFilled = ContainerVolumeFilled;
		if(ContainerContents != null)
		{
			In.ContainerContents = ContainerContents.CloneItem();
		}
		
		In.Fireproof = Fireproof;
		In.Tags =  new List<Tag>(Tags);
		In.Attributes = new List<Attribute>(Attributes);
		In.Coverage = Coverage;
		In.Protection = Protection;
		In.ItemActions = new List<Interaction> (ItemActions);



		In.Encumberance = Encumberance;
		In.ClothingCoverage = ClothingCoverage;

	
		return In;
	}
	
	public bool EatItem()
	{
		if (Tags.Contains (Tag.FoodDrink))
		{
				if (Food > Quench) {
				Global.UIMaster.OutputBox.AddMessage ("you eat the " + DisplayName, TextBoxI.MColor.Green);
				} else {
				Global.UIMaster.OutputBox.AddMessage ("you drink the " + DisplayName, TextBoxI.MColor.Green);
				}
			
				Global.Player.FoodInStomach += Food;
				Global.Player.WaterInStomach += Quench;
				return true;
		} 
		else 
		{
			Global.UIMaster.OutputBox.AddMessage ("you don't think the " + DisplayName + " would taste very good");
				return false;
		}
	}
	
	public static GameObject SpawnItem(Item I,Vector3 Pos,Quaternion Rot)
	{
		GameObject Spawn;
		if(I.WorldObject != null)
		{
			 Spawn = (GameObject)GameObject.Instantiate(I.WorldObject,Pos,Rot);
		}
		else
		{	
			 Spawn = (GameObject)GameObject.Instantiate(DefaultWorldObject,Pos,Rot);
		}
		

		Interactable IN = Spawn.GetComponent<Interactable>();
		ItemSlot Slot = Spawn.GetComponent<ItemSlot>();
		
	
		I = I.CloneItem();

		
		//Interaction_PickUp PU = Spawn.AddComponent<Interaction_PickUp>();
		Slot.Item = I;
		IN.MouseOverName = I.GenerateContainerName();
		
		return Spawn;
		
	}

	
	
}
