﻿using UnityEngine;
using System.Collections;

public class ItemSpawnPoint : MonoBehaviour 
{

	public ItemSpawnList SpawnList;
	// Use this for initialization
	void Awake () 
	{
		if(SpawnList != null)
		{
			if(UnityEngine.Random.Range(0,100) < SpawnList.SpawnChance)
			{
			int i = UnityEngine.Random.Range(0,SpawnList.Items.Length);
			GameObject S = Item.SpawnItem(SpawnList.Items[i],transform.position,transform.rotation);
			S.transform.parent = transform.root;

			}
		}
		
			Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
