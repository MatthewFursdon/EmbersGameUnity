﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Attribute : MonoBehaviour {

	int BaseValue;
	public string Name;
	public List<AttributeModification> Modifications = new List<AttributeModification>();

	public int GetModifiedValue()
	{

		int mval = 0;
		foreach(AttributeModification Mod in Modifications)
		{
			mval+=Mod.Value;
		}

		return BaseValue + mval;

	}

	public int GetBaseValue()
	{
		return BaseValue;
	}

}
