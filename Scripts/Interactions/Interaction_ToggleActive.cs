﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public class Interaction_ToggleActive : Interaction {
	
	
	
	public override void Interact(GameObject Object,Item Item)
	{

		FlashLight F = (FlashLight)Item;

		if (!F.Activated) {
			if (F.HasPower ()) {
				F.Activated = true;
			}
			else
			{
				Global.UIMaster.OutputBox.AddMessage("You switch the " + Item.DisplayName + " on, but it stays dark");
			}
		} else {
			F.Activated = false;
		}

	
		
	}
}
