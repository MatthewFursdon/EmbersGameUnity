﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public abstract class Interaction : ScriptableObject {

	public string Name;

	public abstract void Interact(GameObject Object,Item Used);

}
