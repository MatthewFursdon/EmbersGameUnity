﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Reflection;

[CreateAssetMenuAttribute]
public class Interaction_CutUpClothing : Interaction 
{

	public Item rag;

	public void TaskCompleteCallback()
	{
		Item selection = Global.UIMaster.InventoryWindow.selection;

		Global.UIMaster.OutputBox.AddMessage("You finish cutting the " + selection.DisplayName + " into rags");

		if (Global.Player.Inventory.Contents.Contains (selection)) 
		{
			if (Global.Player.Inventory.RemoveItem (selection)) {
				Global.Player.Inventory.AddItem (rag);
			}
		} 
		else if (Global.Player.Equipment.Contents.Contains (selection)) 
		{
			if (Global.Player.Equipment.RemoveItem (selection)) {
				Global.Player.Inventory.AddItem (rag);
			}
		}


		Global.Player.EndTask();
	}

	public void ItemSelectionCallback()
	{
		//okay object is selected.
		Item selection = Global.UIMaster.InventoryWindow.selection;

		if (selection != null) 
		{

			TimeManager.WaitFailReason Reason = Global.TimeManager.WaitForTime(this.GetType().GetMethod ("TaskCompleteCallback"),this,new System.TimeSpan(0,10,0),10.0f,true);

			if(Reason == TimeManager.WaitFailReason.NoFaliure)
			{
				Global.Player.BeginTask();
				Global.UIMaster.OutputBox.AddMessage("You begin to cut the " + selection.DisplayName + " into rags...");
			}
			else if (Reason == TimeManager.WaitFailReason.NoLight)
			{
				Global.UIMaster.OutputBox.AddMessage("It's too dark to be using something sharp");
			}
		}

	}

	public override void Interact(GameObject Int,Item WieldedItem)
	{



		MethodInfo call = this.GetType().GetMethod ("ItemSelectionCallback");
		//Global.TimeManager.WaitForTime (call, this, new System.TimeSpan (0, 20, 0), 8.0f);
		Global.UIMaster.InventoryWindow.ItemSelectionScreen (call,this);
			

		
	}
}
