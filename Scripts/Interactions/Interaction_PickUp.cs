﻿using UnityEngine;
using System.Collections;


[CreateAssetMenuAttribute]
public class Interaction_PickUp : Interaction {
	

	public override void Interact(GameObject Int,Item WieldedItem)
	{
		ItemSlot Slot = Int.GetComponent<ItemSlot> ();
		if(Slot != null && Slot.Item != null)
		{
			if(Global.Player.Inventory.AddItem(Slot.Item))
			{
				Global.UIMaster.OutputBox.AddMessage("got 1x " + Slot.Item.DisplayName);
				Slot.Item = null;
				Object.Destroy(Int);
			}
			else {
				Global.UIMaster.OutputBox.AddMessage("you're carrying too much",TextBoxI.MColor.Red,true);
			}
		}
	}
}
