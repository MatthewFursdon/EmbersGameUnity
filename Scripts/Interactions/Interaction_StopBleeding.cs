﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public class Interaction_StopBleeding : Interaction 
{

	public override void Interact(GameObject Int,Item WieldedItem)
	{

		if (Int == null) 
		{
			// used on player by player

			if(Global.Player.Head.Bleeding)
			{
				if(Global.Player.Inventory.RemoveItem(WieldedItem))
				{
				Global.Player.Head.Bleeding = false;
					Global.UIMaster.OutputBox.AddMessage("You manage to stop your " + Global.Player.Head.Name +  " bleeding with the " + WieldedItem.DisplayName,TextBoxI.MColor.Green);
				return;
				}
			}

			if(Global.Player.Torso.Bleeding)
			{
				if(Global.Player.Inventory.RemoveItem(WieldedItem))
				{
				Global.Player.Torso.Bleeding = false;
					Global.UIMaster.OutputBox.AddMessage("You manage to stop your " + Global.Player.Torso.Name +  " bleeding with the " + WieldedItem.DisplayName,TextBoxI.MColor.Green);
				return;
				}
			}

			if(Global.Player.Legs.Bleeding)
			{
				if(Global.Player.Inventory.RemoveItem(WieldedItem))
				{
				Global.Player.Legs.Bleeding = false;
					Global.UIMaster.OutputBox.AddMessage("You manage to stop your " + Global.Player.Legs.Name +  " bleeding with the " + WieldedItem.DisplayName,TextBoxI.MColor.Green);
				return;
				}
			}

			if(Global.Player.Arms.Bleeding)
			{
				if(Global.Player.Inventory.RemoveItem(WieldedItem))
				{
				Global.Player.Arms.Bleeding = false;
					Global.UIMaster.OutputBox.AddMessage("You manage to stop your " + Global.Player.Arms.Name +  " bleeding with the " + WieldedItem.DisplayName,TextBoxI.MColor.Green);
				return;
				}
			}

			Global.UIMaster.OutputBox.AddMessage("You aren't hurt badly enough to need a bandage",TextBoxI.MColor.White);
		}


	}
}
