﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public class Interaction_OpenDoor : Interaction {


	
	public override void Interact(GameObject Object,Item Item)
	{
		DoorSubBlock DoorComponent = Object.GetComponent<DoorSubBlock>();
		Debug.LogError(DoorComponent);
		if(DoorComponent != null)
		{
			DoorComponent.OpenOrClose();
		}
		else
		{
			Debug.LogError("ERROR: DOOR OPEN INTERACTION ADDED TO BLOCK WITHOUT SUBBLOCKDOOR SCRIPT");
		}
	
	}
}
