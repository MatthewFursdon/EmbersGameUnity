﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public class Interaction_Unload: Interaction {
	
	
	public override void Interact(GameObject Int,Item WieldedItem)
	{
		if (WieldedItem.Charge1Quantity > 0) 
		{
			Item RemovedBattery = WieldedItem.Charge1.CloneItem();

			if(Global.Player.Inventory.AddItem(RemovedBattery))
			{
				WieldedItem.Charge1Quantity--;
				WieldedItem.Charge1.LifeTime = 1.0f;
				Global.UIMaster.OutputBox.AddMessage("You remove a " + WieldedItem.Charge1.DisplayName + " From the " + WieldedItem.DisplayName);
			}
		}
	}
}
