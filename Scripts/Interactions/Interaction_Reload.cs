﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Reflection;

[CreateAssetMenuAttribute]
public class Interaction_Reload : Interaction 
{
	
	public Item current;
	
	public void TaskCompleteCallback()
	{
		Item selection = Global.UIMaster.InventoryWindow.selection;

		if(Global.Player.Inventory.RemoveItem(selection))
		{
			Global.UIMaster.OutputBox.AddMessage("You insert the " + selection.DisplayName + " into the " + current.DisplayName);
			current.Charge1Quantity++;

			current.Charge1.LifeTime = Mathf.Max(current.Charge1.LifeTime,selection.LifeTime);
		}

		
		Global.Player.EndTask();
	}
	
	public void ItemSelectionCallback()
	{
		//okay object is selected.
		Item selection = Global.UIMaster.InventoryWindow.selection;
		
		if (selection != null) 
		{
			if(selection.DisplayName == current.Charge1.DisplayName)
			{
			
			TimeManager.WaitFailReason Reason = Global.TimeManager.WaitForTime(this.GetType().GetMethod ("TaskCompleteCallback"),this,new System.TimeSpan(0,1,0),1.0f,false);

			Global.Player.BeginTask();
				Global.UIMaster.OutputBox.AddMessage("You begin inserting a new " + selection.DisplayName + " into the " + current.DisplayName);
			}
			else
			{
				Global.UIMaster.OutputBox.AddMessage("The " + selection.DisplayName + " doesn't fit in the " + current.DisplayName);
			}
		}
		
	}
	
	public override void Interact(GameObject Int,Item WieldedItem)
	{
		
		
		current = WieldedItem;

		if (WieldedItem.Charge1Quantity < WieldedItem.Charge1MaxQuantity) {

			MethodInfo call = this.GetType ().GetMethod ("ItemSelectionCallback");
			//Global.TimeManager.WaitForTime (call, this, new System.TimeSpan (0, 20, 0), 8.0f);
			Global.UIMaster.InventoryWindow.ItemSelectionScreen (call, this);
		} else {
			Global.UIMaster.OutputBox.AddMessage("The " + WieldedItem.DisplayName + " has a full set of batteries");
		}
		
		
		
	}
}
