﻿using UnityEngine;
using System.Collections;

[CreateAssetMenuAttribute]
public  class Interaction_Examine : Interaction {


	
	public override void Interact(GameObject Object,Item Item)
	{
		Global.UIMaster.OutputBox.AddMessage(((Interactable)Object.GetComponent<Interactable>()).Description);
	
	}
}
