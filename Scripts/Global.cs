﻿//asd
using UnityEngine;
using System.Collections;

public class Global {

	public static Player Player;
	public static UIMaster UIMaster;
	public static NPCManager NPCManager;
	public static World World;
	public static ItemManager ItemManager;
	public static UnityEngine.Random Random = new Random();
	public static TimeManager TimeManager;
	public static DayNightManager DayNightManager;
}
