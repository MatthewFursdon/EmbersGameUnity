﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Pathfinding;
public class World : MonoBehaviour {

	public static Dictionary<Vector2,Chunk> Chunks = new Dictionary<Vector2,Chunk>();
	public GameObject chunkB;
	public Camera MainCamera;
		public AstarData data;
		
	public int CHUNK_RANGE = 7;
	// Use this for initialization

	void Awake()
	{
		Global.World = this;
	}

	void Start () 
	{
	
	
	
		InvokeRepeating("LoadChunks",0,1);
		InvokeRepeating("UnloadChunks",5,5);
		
		//LoadChunk(0,0);
	}

	void AddChunk(int x,int y)
	{
		Vector3 offset = new Vector3(x*Chunk.CHUNK_SIZE*Block.BLOCK_SIZE,0,y*Chunk.CHUNK_SIZE*Block.BLOCK_SIZE);
		Chunk NewChunk = ((GameObject)(GameObject.Instantiate(chunkB,offset,Quaternion.identity))).GetComponent<Chunk>();
		Chunks.Add(new Vector2(x,y),((NewChunk)));
		NewChunk.ChunkPosition = new Vector2(x,y);

	}
	// Update is called once per frame

	void UnloadChunks()
	{
		//Debug.Log("Cleaning up chunks");
		List<Chunk> CToRemove = new List<Chunk>();
		foreach(Chunk C in Chunks.Values)
		{
			if(!ChunkInRange(C.ChunkPosition,MainCamera.transform.position,(CHUNK_RANGE+2)))
			{
				//Debug.Log("Removing chunk: " + C.ChunkPosition.ToString());
				CToRemove.Add(C);

			}

		}

		foreach(Chunk C in CToRemove)
		{
			Chunks.Remove(C.ChunkPosition);
			Object.Destroy(C.gameObject);
		}

		LoadChunks();
	}

	public bool ChunkInRange(Vector2 ChunkPosition,Vector3 Position, float range)
	{
		int Cx = (int)Mathf.Floor(Position.x/((float)(Chunk.CHUNK_SIZE*Block.BLOCK_SIZE)));
		int Cz = (int)Mathf.Floor(Position.z/((float)(Chunk.CHUNK_SIZE*Block.BLOCK_SIZE)));

		Vector2 C = new Vector2(Cx,Cz);


		if((C-ChunkPosition).magnitude > range)
		{
			return false;
		}
		return true;
	}

	public int[,,] LoadChunkData(int x,int y)
	{
		
		int[,,] data = new int[Chunk.CHUNK_SIZE, 5, Chunk.CHUNK_SIZE];
		byte[,,] rotation = new byte[Chunk.CHUNK_SIZE, 5, Chunk.CHUNK_SIZE];
		
		string FileName = "Chunk"+x+"-"+y+".txt";
		StreamReader swOverwrite = new StreamReader(".\\Chunks\\"+FileName);
		
		
		int Px = 0;
		int Pz = 0;
		int i = 0;
		char[] delim = new char[]{':'};
		int Py = 0;
		while(Py < Chunk.Z_LEVELS)
		{
			string S = swOverwrite.ReadLine();
			
			string[] Sstrings =  S.Split(delim);
			
			while(Px < Chunk.CHUNK_SIZE)
			{
				while(Pz < Chunk.CHUNK_SIZE)
				{
					string[] parts =  Sstrings[i].Split('/');
					int dat = int.Parse(parts[0]);
					byte rot = byte.Parse(parts[1]);
					data[Px,Py,Pz] = dat;
					rotation[Px,Py,Pz] = rot;
					i++;
					Pz++;
				}
				Pz = 0;
				Px++;
			}
			Px = 0;
			Pz = 0;
			i = 0;
			Py++;
		}
	
		return data;
	}


	Chunk LoadChunk(int x,int y)
	{

		Vector3 offset = new Vector3(x*Chunk.CHUNK_SIZE*Block.BLOCK_SIZE,0,y*Chunk.CHUNK_SIZE*Block.BLOCK_SIZE);
		Chunk NewChunk = ((GameObject)(GameObject.Instantiate(chunkB,offset,Quaternion.identity))).GetComponent<Chunk>();
		Chunks.Add(new Vector2(x,y),((NewChunk)));
		NewChunk.ChunkPosition = new Vector2(x,y);



		string FileName = "Chunk"+x+"-"+y+".txt";
		StreamReader swOverwrite = new StreamReader(".\\Chunks\\"+FileName);


		int Px = 0;
		int Pz = 0;
		int i = 0;
		char[] delim = new char[]{':'};
		int Py = 0;
		while(Py < Chunk.Z_LEVELS)
		{
		string S = swOverwrite.ReadLine();
	
		string[] Sstrings =  S.Split(delim);
		
		while(Px < Chunk.CHUNK_SIZE)
		{
			while(Pz < Chunk.CHUNK_SIZE)
			{
				string[] parts =  Sstrings[i].Split('/');
				int dat = int.Parse(parts[0]);
				byte rot = byte.Parse(parts[1]);
				NewChunk.BlockData[Px,Py,Pz] = dat;
				NewChunk.RotationData[Px,Py,Pz] = rot;
				i++;
				Pz++;
			}
			Pz = 0;
			Px++;
		}
		Px = 0;
		Pz = 0;
		i = 0;
		Py++;
		}
		NewChunk.CreateBlocks();
		return NewChunk;
	}

	public static bool PositionIsValid(int x, int z)
	{
		return (x>=0 && x<GenerateWorld.WorldSize*Chunk.CHUNK_SIZE)&&(z>=0 && z<GenerateWorld.WorldSize*Chunk.CHUNK_SIZE);
	}


	public static Block GetBlock(Vector3 P)
	{
	
		return GetBlock((int)P.x,(int)P.y,(int)P.z);
		
	}

	public static Block GetBlock(int x,int y,int z)
	{
		if(PositionIsValid(x,z))
		{
		//calculate the chunk it's in
		int Cx = (int)Mathf.Floor(x/((float)(Chunk.CHUNK_SIZE)));
		int Cz = (int)Mathf.Floor(z/((float)(Chunk.CHUNK_SIZE)));
		int Bx = x-(Cx)*Chunk.CHUNK_SIZE;
		int Bz = z-(Cz)*Chunk.CHUNK_SIZE;

		
		//return the block from that chunk
		Vector2 CV = new Vector2(Cx,Cz);
		if(Chunks.ContainsKey(CV))
		{
			Block B = Chunks[CV].Blocks[Bx,y,Bz];
			return B;
		}
		}
		return null;

	}

	void LoadChunks () 
	{

		bool loadedanything = false;
		
		int ChunksToLoad = CHUNK_RANGE; // 
		Vector3 CameraPos = MainCamera.transform.position;
		int Cx = (int)Mathf.Floor(CameraPos.x/((float)(Chunk.CHUNK_SIZE*Block.BLOCK_SIZE)));
		int Cz = (int)Mathf.Floor(CameraPos.z/((float)(Chunk.CHUNK_SIZE*Block.BLOCK_SIZE)));

		int sx = Cx - ChunksToLoad;
		int sz = Cz - ChunksToLoad;

		if(sx < 0)
		{
			sx = 0;
		}

		if(sz < 0)
		{
			sz = 0;
		}

		int szi = sz;

		while(sx < Cx+ChunksToLoad)
		{	
			while(sz < Cz+ChunksToLoad)
			{
				Vector2 CV = new Vector2(sx,sz);
				if(!Chunks.ContainsKey(CV))
				{
					loadedanything = true;
					LoadChunk(sx,sz);
				
				}
				sz++;
			}
			sx++;
			sz = szi;
		}

		if(loadedanything)
		{

		foreach(Chunk C in Chunks.Values)
		{
			C.ReCheckNeighbors();
		}
		
	    data = AstarPath.active.astarData;
		
		GridGraph GG = data.graphs[0] as GridGraph;
	
		GG.center = MainCamera.transform.position-new Vector3(0,30,0);
		AstarPath.active.Scan();
		

		}
	}
}
