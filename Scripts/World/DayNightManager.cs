﻿using UnityEngine;
using System.Collections;

public class DayNightManager : MonoBehaviour {



	Material SkyboxM;

	public SkyState Day;
	public SkyState Night;
	public SkyState Dawn;
	public SkyState Dusk;

	SkyState CurrentState;
	SkyState TransitionTo;
	System.TimeSpan Remaining;
	System.TimeSpan InitialRemaining;


	public Light DirectionalLight;

	// Use this for initialization
	void Start () {

		Global.DayNightManager = this;
		SkyboxM = RenderSettings.skybox;
	//	CurrentState = Day;
	//	SetSkyState (Day);
	}

	public void SetSkyState(SkyState State)
	{
		SkyboxM.SetColor("_SkyTint",State.SkyTint);
		SkyboxM.SetColor("_GroundColor",State.GroundTint);
		SkyboxM.SetFloat("_Exposure",State.Exposure);
		RenderSettings.ambientLight = State.AmbientLight;
		DirectionalLight.intensity = State.DirectionalLightIntensity;
		CurrentState = State;
		TransitionTo = State;
	}

	public void SetSkyState(SkyState State,System.TimeSpan Time)
	{
		if (TransitionTo != State) {
			TransitionTo = State;
			Remaining = Time;
			InitialRemaining = Time;
		}
	}

	public void TickTime(System.TimeSpan time)
	{
		Remaining = Remaining.Subtract (time);
	}

	// Update is called once per frame
	void Update () 
	{
		if(false && CurrentState != TransitionTo)
		{


			System.TimeSpan T = InitialRemaining.Subtract (Remaining);
			float t = (float)T.TotalSeconds / (float)InitialRemaining.TotalSeconds;
			SkyboxM.SetColor("_SkyTint",Color.Lerp(CurrentState.SkyTint,TransitionTo.SkyTint,t));
			SkyboxM.SetColor("_GroundColor",Color.Lerp(CurrentState.GroundTint,TransitionTo.GroundTint,t));
			SkyboxM.SetFloat("_Exposure",Mathf.Lerp(CurrentState.Exposure,TransitionTo.Exposure,t));
			RenderSettings.ambientLight = Color.Lerp(CurrentState.AmbientLight,TransitionTo.AmbientLight,t);
			DirectionalLight.intensity = Mathf.Lerp(CurrentState.DirectionalLightIntensity,TransitionTo.DirectionalLightIntensity,t);
		//	Debug.Log (t);
			if(t > 0.95f)
			{
				SetSkyState(TransitionTo);
			}
		}

	}
}
