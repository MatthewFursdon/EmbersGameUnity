﻿using UnityEngine;
using System.Collections;

public class SkyState : MonoBehaviour 
{

	public Color SkyTint;
	public Color GroundTint;

	public float Exposure;
	public float AtmosphericThickness;

	public Color AmbientLight;
	public Color DirectionalLight;
	public float DirectionalLightIntensity;


}
