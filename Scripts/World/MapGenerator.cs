﻿using UnityEngine;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
public class MapGenerator : MonoBehaviour {

	LockBitmap MapImage;
	Bitmap IMap;
	Size CSize;

	public List<UnityEngine.Color> BlockColors; 
	public List<System.Drawing.Color> InternalBlockColors; 


	System.Drawing.Color CColorFromUnityColor(UnityEngine.Color cu)
	{
		int r = (int)(Mathf.Round (cu.r*255.0f));
		int g = (int)(Mathf.Round (cu.g*255.0f));
		int b = (int)(Mathf.Round (cu.b*255.0f));
		int a = (int)(Mathf.Round (cu.a*255.0f));
		return System.Drawing.Color.FromArgb(a,r, g, b);
	}

	void Start()
	{
		GenerateMapImage ();
	}

	public void GenerateMapImage()
	{

		foreach (UnityEngine.Color Cu in BlockColors) {
			InternalBlockColors.Add(CColorFromUnityColor(Cu));
		}

		IMap = new Bitmap (GenerateWorld.WorldSize * Chunk.CHUNK_SIZE, GenerateWorld.WorldSize * Chunk.CHUNK_SIZE);
		MapImage = new LockBitmap (IMap);

		CSize = new Size (Chunk.CHUNK_SIZE, Chunk.CHUNK_SIZE);
		int Cx = 0;
		int Cz = 0;
		MapImage.LockBits ();
		while (Cz < GenerateWorld.WorldSize) 
		{

			while (Cx < GenerateWorld.WorldSize) 
			{
				int[,,] C = Global.World.LoadChunkData(Cx,Cz);

				if(C != null)
				{
					GenerateChunkPixels(C,Cx*Chunk.CHUNK_SIZE,Cz*Chunk.CHUNK_SIZE);
				}

				Cx++;
			}
			Cz++;
			Cx = 0;
		}

		MapImage.UnlockBits ();
		IMap.RotateFlip (RotateFlipType.RotateNoneFlipY);
		IMap.Save("test.png",ImageFormat.Png);


	}

	
	int GetMapHighestPoint(int[,,] map, int x,int z)
	{
		int y = 4;

		while (y >= 0)
		{
			if(map[x,y,z] != 0)
			{
				//Debug.Log(map[x,y,z]);
				return map[x,y,z];
			}
			y--;
		}
		return map [x, 0, z];
	}


	public void GenerateChunkPixels(int[,,] C,int X,int Z)
	{
		Point Cpos = new Point((int)X*Chunk.CHUNK_SIZE,(int)Z*Chunk.CHUNK_SIZE);
		Rectangle LockRect = new Rectangle(Cpos,CSize);
	
		//MapImage.LockBits ();
		MapImage.SetPixel(0,0,System.Drawing.Color.Pink);
		MapImage.SetPixel(1,0,System.Drawing.Color.Pink);
		MapImage.SetPixel(1,1,System.Drawing.Color.Pink);
		MapImage.SetPixel(0,1,System.Drawing.Color.Pink);
		int x = 0;
		int z = 0;

		while (x < Chunk.CHUNK_SIZE) 
		{
			while (z < Chunk.CHUNK_SIZE) 
			{
				MapImage.SetPixel(X+x,Z+z,InternalBlockColors[GetMapHighestPoint(C,x,z)]);
				z++;
			}
			z = 0;
			x++;
		}



	}

}