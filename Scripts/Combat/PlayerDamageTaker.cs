﻿using UnityEngine;
using System.Collections;

public class PlayerDamageTaker : DamageTaker {

	public Player Owner;



	public override void TakeDamage(Item.DamageTypes DamageType,float Damage,float Roll)
	{
		// roll for bodypart (probably not the place to do this, oh well)

		int bodypart = Random.Range (0, 4);
		Bodypart HitPart; 

		if (bodypart == 1) {
			HitPart = Owner.Head;
		}
		else if (bodypart == 2) {
			HitPart = Owner.Torso;
		}
		else if (bodypart == 3) {
			HitPart = Owner.Arms;
		}
		else {
			HitPart = Owner.Legs;
		}

		float amour = HitPart.CalulateAmour (DamageType);

		Global.UIMaster.OutputBox.AddMessage("enemy roll: " + Roll + " Amour: " + amour);

		float finaldamage = Damage - amour;
		if (finaldamage < 0) {
			finaldamage = 0;

			//Global.UIMaster.OutputBox.AddMessage("You are hit for " + Damage + " Damage",TextBoxI.MColor.Red,true);
			Global.UIMaster.OutputBox.AddMessage("The blow hits your " + HitPart.Name + " but your amour absorbs it",TextBoxI.MColor.Red);
			//Owner.Attacked(DamageType,Damage);

		}
		else
		{
			//Global.UIMaster.OutputBox.AddMessage("You are hit for " + Damage + " Damage",TextBoxI.MColor.Red,true);
			Global.UIMaster.OutputBox.AddMessage("Your" + HitPart.Name + " " + HitPart.Verb + " hit for " + finaldamage + " Damage",TextBoxI.MColor.Red);
			//this.health -= finaldamage;
			//Global.UIMaster.TemporaryHealth.SetValue(this.health / 100.0f);
			Owner.Attacked(DamageType,finaldamage,HitPart);

		}


	}
}	
