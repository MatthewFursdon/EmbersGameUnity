﻿using UnityEngine;
using System.Collections;

public class DamageTaker : MonoBehaviour {

	public float health = 100.0f;

	public virtual void TakeDamage(Item.DamageTypes DamageType,float Damage,float Roll)
	{
		this.health -= Damage;
	}
	
	public float CalculateResistance(Item.DamageTypes DamageType,float Damage,float Roll)
	{
		return Damage;
		
	}
}
