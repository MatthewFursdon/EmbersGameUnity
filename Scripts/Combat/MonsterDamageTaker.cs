﻿using UnityEngine;
using System.Collections;

public class MonsterDamageTaker : DamageTaker {
	
	public NPC Owner;

	public override void TakeDamage(Item.DamageTypes DamageType,float Damage,float Roll)
	{

		this.health -= Damage;
		Owner.Damaged(DamageType,Damage);
			
	}
}
