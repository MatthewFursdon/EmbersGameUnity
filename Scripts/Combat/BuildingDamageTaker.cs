﻿using UnityEngine;
using System.Collections;

public class BuildingDamageTaker : DamageTaker {

	public override void TakeDamage(Item.DamageTypes DamageType,float Damage,float Roll)
	{

		// make some noise!

		Global.NPCManager.SpawnNPCNoise (transform.position, 10 * Block.BLOCK_SIZE, 1.0f);
		Global.UIMaster.OutputBox.AddMessage ("thud!", TextBoxI.MColor.Yellow);

		this.health -= Damage;
		if(health <= 0)
		{
			gameObject.active = false;
			Global.NPCManager.SpawnNPCNoise (transform.position, 30 * Block.BLOCK_SIZE, 1.0f);
			Global.UIMaster.OutputBox.AddMessage ("SMASH!", TextBoxI.MColor.Red,true);
		}
	}
	
}
