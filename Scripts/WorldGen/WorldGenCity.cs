﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldGenCity  {

	public List<Worldgen_Road> roads;
	public List<WorldGenBuilding> buildings;

	Bounds citybounds;
	Vector3 centerofmass;

	public Vector3 CalculateCenterOfMass()
	{
		return Vector3.zero;
	}

	public Bounds CalculateCityBounds()
	{
		return new Bounds(Vector3.zero,Vector3.zero);
	}

}
