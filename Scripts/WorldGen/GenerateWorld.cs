﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using System.Threading;
public class GenerateWorld : MonoBehaviour {

	// Generate and save a new world

	public int[, ,] world = new int[WorldSize*Chunk.CHUNK_SIZE,Chunk.Z_LEVELS,WorldSize*Chunk.CHUNK_SIZE];
	public byte[, ,] RotationData = new byte[WorldSize*Chunk.CHUNK_SIZE,Chunk.Z_LEVELS,WorldSize*Chunk.CHUNK_SIZE];
	public List<BuildingGen> TownGenerators = new List<BuildingGen>();
	public List<BuildingGen> SuburbGenerators = new List<BuildingGen>();
	public static int WorldSize = 200;
	public bool Regenerate;
	public static Chunk ChunkDef;


	
	public List<BuildingGen> FindUsableGenerators(int x, int z,BuildingGen.Rotation Front,List<BuildingGen> bg)
	{
		Vector2 space = BuildingGen.FindBuildableArea(x,1,z,Front,this);
		List<BuildingGen> Usable = new List<BuildingGen>();
		foreach(BuildingGen B in bg)
		{
			Vector2 dif = B.MinimumSize - space;
			//Debug.Log( B.MinimumSize + " " + space + " " + dif);
			if(dif.x < 0 && dif.y < 0)
			{
			//usable
			Usable.Add(B);
			}
		}	
		
		return Usable;
	
	}
	
	public void PlaceRandomBuilding(int x,int z,BuildingGen.Rotation Front,List<BuildingGen> bg)
	{
		Vector2 space = BuildingGen.FindBuildableArea(x,0,z,Front,this);
		List<BuildingGen> buildings = FindUsableGenerators(x,z,Front,bg);
		int i = Random.Range(0,buildings.Count-1);
		
		if(buildings.Count > 0)
		{
				buildings[i].Place(x,z,(int)space.x,(int)space.y,Front,this);
		}
	}
	
	public void GenerateNewWorld() 
	{


			if(Directory.Exists(".\\Chunks"))
			{
			Directory.Delete(".\\Chunks",true);
			}
			Thread.Sleep(1000);
			Directory.CreateDirectory(".\\Chunks");

		int x = 0;
		int y = 0;

		while(x < WorldSize*Chunk.CHUNK_SIZE)
		{
			while(y < WorldSize*Chunk.CHUNK_SIZE)
			{
			
		
			//	if(Random.Range(0,100) < 5)
			//	{
			//		world[x,0,y] = 1;
			//	}
			//	else
			//	{
					world[x,0,y] = 1;
		
			//	}
				y++;
			}
			x++;
			y = 0;
		}
		PlaceCity();
		SaveIntoChunks();
	
	}

	Worldgen_Road PlaceHRoad(List<BuildingGen> bg)
	{
		int length = Random.Range(50,200);
		
		int x = Random.Range(0,2000);
		int z = Random.Range(0,2000); 
		return PlaceHRoad(x,z,length,bg);
	}
	
	Worldgen_Road PlaceHRoad(int x,int z,int length,List<BuildingGen> bg)
	{
	
		Worldgen_Road Road = new Worldgen_Road(new Vector2(x,z),length,Worldgen_Road.RoadDirection.Horizontal,bg);
		
			int XStart = x;
			int XEnd = x+length;
			
			if(length < 0)
			{
				XStart = x+length;
				XEnd = x;
			}
			
			int xt = XStart;
			int t = XStart;
			
		
		if(SquareOnMap(x,z,length+50,4,WorldSize*Chunk.CHUNK_SIZE))
		{
	
			while(t < XEnd)
			{
				int j = -16;
				
				while(j <= 16)
				{
					if(IsHRoad(t,z+j))
					{
						return null;
					}
					j++;
				}
			t++;
			}
			

			while(xt < XEnd)
			{
				world[xt,0,z-2] = 17;
				world[xt,0,z-1] = 8;
				world[xt,0,z] = 9;
				world[xt,0,z+1] = 10;
				world[xt,0,z+2] = 17;
				
				Road.tiles.Add(new Vector2(xt,z-2));
				Road.tiles.Add(new Vector2(xt,z-1));
				Road.tiles.Add(new Vector2(xt,z));
				Road.tiles.Add(new Vector2(xt,z+1));
				Road.tiles.Add(new Vector2(xt,z+2));
				
				Road.CenterTiles.Add(new Vector2(xt,z));




				xt++;
			}
		}
		return Road;

	}

	Worldgen_Road PlaceVRoad(int x,int z,int length,List<BuildingGen> bg)
	{
		
		Worldgen_Road Road = new Worldgen_Road(new Vector2(x,z),length,Worldgen_Road.RoadDirection.Vertical,bg);

		int ZStart = z;
		int ZEnd = z+length;
		
		if(length < 0)
		{
			ZStart = z+length;
			ZEnd = z;
		}
		
		int zt = ZStart;
		int t = ZStart;
		
		
		if(SquareOnMap(x,z,4,length+50,WorldSize*Chunk.CHUNK_SIZE))
		{
			
			while(t < ZEnd)
			{
				int j = -16;
				
				while(j <= 16)
				{
					if(IsVRoad(x+j,t))
					{
						return null;
					}
					j++;
				}
				t++;
			}
			
			
			while(zt < ZEnd)
			{
				world[x-2,0,zt] = 17;
				world[x-1,0,zt] = 11;
				world[x,0,zt] = 12;
				world[x+1,0,zt] = 13;
				world[x+2,0,zt] = 17;
				
				Road.tiles.Add(new Vector2(x-2,zt));
				Road.tiles.Add(new Vector2(x-1,zt));
				Road.tiles.Add(new Vector2(x,zt));
				Road.tiles.Add(new Vector2(x+1,zt));
				Road.tiles.Add(new Vector2(x+2,zt));
				
				Road.CenterTiles.Add(new Vector2(x,zt));

				
				zt++;
			}
		}
		return Road;
		
	}

	
	int attempt = 0;
	public void PlaceSideRoads(List<Worldgen_Road> roads,Worldgen_Road R1,int I,List<BuildingGen> bg)
	{
		int roadcount = Random.Range(2,5);
		
		if(I == 0)
		{
		roadcount = Random.Range(3,9);
		}
		
		int j = 0;
		
		while(j < roadcount)
		{
	
		if(R1.Direction == Worldgen_Road.RoadDirection.Horizontal && R1.CenterTiles.Count > 0)
		{
				Restart1:
					if(attempt == 10)
				{
					attempt = 0;
					return;
				}
				int length = -1*Random.Range(50,100);

				if(bg == SuburbGenerators)
				{
					length = -1*Random.Range(80,200);
				}
				
				if(Random.Range(0,100) >= 50)
				{
					length = -1*length;
				}
				int index = Random.Range(0,R1.CenterTiles.Count-1);
				Vector2 RandomPos = R1.CenterTiles[index];
				
				foreach(int pos in R1.BranchPoints)
				{
					if(Mathf.Abs(RandomPos.x - pos) < 10)
					{
						attempt++;
						goto Restart1;
						
					}
					
				}
				
				
				
				attempt = 0;
				int mod = 2;
				if(length < 0)
				{
					mod = -2;
				}
				Worldgen_Road Road = PlaceVRoad( (int)RandomPos.x,(int)RandomPos.y+mod,length,bg);


				if(Road != null)
				{
					R1.BranchPoints.Add((int)RandomPos.x);
					Road.bg = bg;
					roads.Add(Road);
					R1.ConnectingRoads.Add(Road);
					Debug.Log("Side Road Added!");
				}

		}
		else if(R1.Direction == Worldgen_Road.RoadDirection.Vertical && R1.CenterTiles.Count > 0)
		{
			Restart:
			if(attempt == 10)
			{
			attempt = 0;
			return;
			}
			int length = -1*Random.Range(50,100);
			
			if(bg == SuburbGenerators)
			{
				length = -1*Random.Range(80,200);
			}

			if(Random.Range(0,100) >= 50)
			{
			length = -1*length;
			}

			

			int index = Random.Range(0,R1.CenterTiles.Count-1);
			Vector2 RandomPos = R1.CenterTiles[index];
				
			foreach(int pos in R1.BranchPoints)
			{
				if(Mathf.Abs(RandomPos.y - pos) < 10)
				{
				attempt++;
				goto Restart;
				
				}
				
			}
			
	
		
			attempt = 0;
			int mod = 2;

			if(length < 0)
			{
			mod = -2;
			}
			Worldgen_Road Road = PlaceHRoad( (int)RandomPos.x+mod,(int)RandomPos.y,length,bg);
			
			if(Road != null)
			{
					Road.bg = bg;
					R1.BranchPoints.Add((int)RandomPos.y);
			roads.Add(Road);
			R1.ConnectingRoads.Add(Road);
			Debug.Log("Side Road Added!");
			}
			else
			{
			
			}
		}
	
		j++;
		
		}

	
	}

	public void PathPointToRoad(Vector2 point,Worldgen_Road road,List<BuildingGen> bg)
	{
		Vector3 endpoint = road.CenterTiles [Random.Range (0, road.CenterTiles.Count)];

		Vector2 dpos = new Vector2(endpoint.x - point.x,endpoint.y - point.y);

		PlaceHRoad ((int)point.x, (int)point.y, (int)dpos.x,bg);
		PlaceVRoad ((int)endpoint.x, (int)point.y, (int)dpos.y,bg);
		 

	}
	
	public void PlaceBuildings(Worldgen_Road Road,List<BuildingGen> bg)
	{
		foreach(Vector2 V in Road.CenterTiles)
		{
			if(Random.Range(0,100)>= 20)
			{
				if(Road.Direction == Worldgen_Road.RoadDirection.Horizontal)
				{
					PlaceRandomBuilding((int)V.x,(int)V.y+3,BuildingGen.Rotation.South,bg);
				}
				else
				{
					PlaceRandomBuilding((int)V.x-3,(int)V.y,BuildingGen.Rotation.East,bg);
				}
			
			}
			
			if(Random.Range(0,100)>= 20)
			{
				if(Road.Direction == Worldgen_Road.RoadDirection.Horizontal)
				{
					PlaceRandomBuilding((int)V.x,(int)V.y-3,BuildingGen.Rotation.North,bg);
				}
				else
				{
					PlaceRandomBuilding((int)V.x+3,(int)V.y,BuildingGen.Rotation.West,bg);
				}
			
			}
		}
	
	}
	
	public void PlaceCity()
	{

		//PlaceRandomBuilding (520, 520, BuildingGen.Rotation.South, SuburbGenerators);
		//PlaceRandomBuilding (550, 550, BuildingGen.Rotation.East, SuburbGenerators);
		//PlaceRandomBuilding (580, 580, BuildingGen.Rotation.West, SuburbGenerators);
		//PlaceRandomBuilding (610, 610, BuildingGen.Rotation.North, SuburbGenerators);

	
	// first place 'main road'
	List<Worldgen_Road> roads = new List<Worldgen_Road>();
	int I = 0;
	Worldgen_Road R1 = PlaceHRoad( 500,500,250,TownGenerators);

		//PlaceRandomBuilding (520, 520, BuildingGen.Rotation.South, SuburbGenerators);

	
	PathPointToRoad (new Vector2 (200, 200), R1,SuburbGenerators);
	PathPointToRoad (new Vector2 (600, 830), R1,SuburbGenerators);

	roads.Add(R1);



	

	
	
	PlaceSideRoads(roads,R1,I,TownGenerators);	
	I++;
	
	
	int iterations = 0;
	

	foreach(Worldgen_Road Road in new List<Worldgen_Road>(roads))
	{
		if(Road.ConnectingRoads.Count == 0)
		{
			PlaceSideRoads(roads,Road,I,TownGenerators);	
		}
	
	}

		foreach(Worldgen_Road Road in roads)
		{
			if(!Road.BuildingsGenerated)
			{
				PlaceBuildings(Road,Road.bg);
				Road.BuildingsGenerated = true;
			}
		}
		
	while(iterations < 3)
	{
			
		foreach(Worldgen_Road Road in new List<Worldgen_Road>(roads))
		{
			if(Road.ConnectingRoads.Count == 0)
			{
				PlaceSideRoads(roads,Road,I,SuburbGenerators);	
			}
				
		}
			
		iterations++;
			
		}

	foreach(Worldgen_Road Road in new List<Worldgen_Road>(roads))
	{
		if(Road.bg == TownGenerators)
		{
			PlaceSideRoads(roads,Road,I,TownGenerators);	
		}
			
	}
	
	foreach(Worldgen_Road Road in roads)
	{
		if(!Road.BuildingsGenerated)
		{
			PlaceBuildings(Road,Road.bg);
			Road.BuildingsGenerated = true;
		}
	}
	
	
	}

	public bool IsRoad(int x,int z)
	{
	int a = world[x,0,z];
	return( a == 8 || a == 9 || a == 10 || a == 11 || a == 12 || a == 13 || a == 17);
	
	}
	
	public  bool IsHRoad(int x,int z)
	{
	int a = world[x,0,z];
	return( a == 8 || a == 9 || a == 10);
	
	}
	
	public bool IsVRoad(int x,int z)
	{
	int a = world[x,0,z];
	return(  a == 11 || a == 12 || a == 13);
	
	}

	


	public bool SquareOnMap(int x,int z, int width,int height, int MapSize)
	{
		if(x>=0 && z >= 0)
		{
			return(x+width<MapSize && z+height<MapSize && x+width>=0 && z+height>=0);
		}
		return false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void SaveIntoChunks()
	{
		int x = 0;
		int z = 0;

		int Cx = 0;
		int Cz = 0;


		while(Cx < WorldSize)
		{
			while(Cz < WorldSize)
			{

				FileStream fsOverwrite = new FileStream(".\\Chunks\\Chunk" + Cx + "-" + Cz + ".txt",FileMode.Create);
				StreamWriter swOverwrite = new StreamWriter(fsOverwrite);

			
				 x = 0;
				 z = 0;
				int y = 0;
				while(y < Chunk.Z_LEVELS)
				{
				
				while(x < Chunk.CHUNK_SIZE)
				{
					while(z < Chunk.CHUNK_SIZE)
					{
		
							swOverwrite.Write(world[x+Cx*Chunk.CHUNK_SIZE,y,z+Cz*Chunk.CHUNK_SIZE]+"/"+RotationData[x+Cx*Chunk.CHUNK_SIZE,y,z+Cz*Chunk.CHUNK_SIZE]+":");

					
						z++;
					}
					swOverwrite.Flush();
					z = 0;
					x++;
				}
				swOverwrite.Write ("\r\n");
				swOverwrite.Flush();
				x=0;
				z=0;
				y++;
				}
				swOverwrite.Write ("\r\n");
				swOverwrite.Flush();
				swOverwrite.Close();
				Cz++;
			
			
			}
			Cx++;
			Cz=0;
			

		}


	Object.Destroy(this.gameObject);

	}
}
