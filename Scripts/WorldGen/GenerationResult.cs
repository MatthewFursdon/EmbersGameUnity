﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerationResult 
{
	public int[,,] WorkingSpace;
	public Dictionary<Vector3,int> ExtraBlocks = new Dictionary<Vector3, int>();

	public GenerationResult(int [,,] WS)
	{
		WorkingSpace = WS;
	}

}
