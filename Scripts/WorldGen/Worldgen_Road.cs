﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Worldgen_Road 
{

	public Vector2 Position;
	public int width = 3;
	public int length;

	public List<Vector2> tiles = new List<Vector2>(); // list of all tiles that make up road
	public List<Vector2> CenterTiles = new List<Vector2>(); // list of center tiles
	public List<int> BranchPoints = new List<int>(); 
	public List<Worldgen_Road> ConnectingRoads = new List<Worldgen_Road>();
	public enum RoadDirection{Horizontal,Vertical};
	public RoadDirection Direction;
	public bool BuildingsGenerated = false;
	public List<BuildingGen> bg;

	public Worldgen_Road(Vector2 pos,int Length,RoadDirection Dir,List<BuildingGen> b)
	{
	Direction = Dir;
	Position = pos;
	length = Length;
		bg = b;
	}
	


}
