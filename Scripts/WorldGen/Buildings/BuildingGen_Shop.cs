﻿using UnityEngine;
using System.Collections;

public class BuildingGen_Shop : BuildingGen
{

	public virtual void GenerateShelves(int XEntrance,int XSize,int ZSize,int[,,] WorkingSpace,byte[,,] RotationSpace,Rotation Front,GenerateWorld World)
	{

		int ShelfRows = (int)Mathf.Floor((ZSize - 5.0f));
		int shelfi = 0;
		
		while(shelfi < ShelfRows)
		{
			
			SetXLine(false,2,XSize-2,2,2+(shelfi),20,WorkingSpace,World);
		//	SetXLineRotation(false,2,XSize-2,2,2+(shelfi),Front,RotationSpace,World);
			shelfi+=2;
		}
		shelfi-=2;
		SetZLine(false,2,2+(shelfi)+1,2,XEntrance-1,18,WorkingSpace,World); 
		SetZLine(false,2,2+(shelfi)+1,2,XEntrance,18,WorkingSpace,World); 

	}


	public void CreateAlly(int x,int z,int Width,int Height,Rotation Front,int[,,] WorkingSpace,byte[,,] RotationSpace,GenerateWorld world)
	{
		//Vector2 left = workingToWorld(x,z,RandPointOnZLine(1,Height,1),Width,Height,Front);
		//Vector2 right = workingToWorld(x,z,RandPointOnZLine(1,Height,Width),Width,Height,Front);

	//	SetXLine(true,(int)left.x-10,(int)left.x,1,(int)left.y,22,null,world);
	//	SetXLine(true,(int)right.x,(int)right.x+10,1,(int)right.y,22,null,world);
		// left

		if (Front == Rotation.South) 
		{

			Vector2 left = workingToWorld (x, z, RandPointOnZLine (1, Height, 1), Width, Height, Front);
			int j = 1;

			if(Random.Range(0,100) <= 60)
			{

			while (j < 10) {
				if (world.world [(int)left.x - j, 2, (int)left.y] == 22 || world.world [(int)left.x - j, 2, (int)left.y] == 4) {
					SetXLine (true, (int)left.x - j +1, (int)left.x, 2, (int)left.y, 22, null, world);
					//Debug.Log ("fence built " + left);
					break;
				}
				j++;
			}
		
			}

			// right


			if(Random.Range(0,100) <= 60)
			{


			j = 1;
			Vector2 right = workingToWorld (x, z, RandPointOnZLine (1, Height, Width), Width, Height, Front);
			while (j < 10) {
				if (world.world [(int)right.x + j, 2, (int)right.y] == 22 || world.world [(int)right.x + j, 2, (int)right.y] == 4) {
					SetXLine (true, (int)right.x, (int)right.x + j - 1, 2, (int)right.y, 22, null, world);
					//Debug.Log ("fence built " + right);
					break;
				}
				j++;
			}

			}

			// back

			if(Random.Range(0,100) <= 100)
			{
				
				
				j = 1;
				Vector2 back = workingToWorld (x, z, RandPointOnXLine (1, Width, Height), Width, Height, Front);
				while (j < 10) {
					if (world.world [(int)back.x, 2, (int)back.y + j] == 22 || world.world [(int)back.x, 2, (int)back.y + j] == 4) {
						SetZLine (true, (int)back.y, (int)back.y + j - 1, 2, (int)back.x, 22, null, world);
						Debug.Log ("fence built " + back);
						break;
					}
					j++;
				}
				
			}

		}
	}

	public override void PostStamp (int x, int z, int Width, int Height, Rotation Front, int[,,] WorkingSpace,byte[,,] RotationSpace, GenerateWorld World)
	{
		CreateAlly (x, z, Width, Height, Front, WorkingSpace,RotationSpace, World);
	}

	public override void Place(int x,int z,int MaxWidth,int MaxHeight,Rotation Front,GenerateWorld World)
	{
		int MaxX = (int)Mathf.Min(MaxWidth,MaximumSize.x);
		int MaxZ = (int)Mathf.Min(MaxHeight,MaximumSize.y);
		
		int XSize = (int)Random.Range(MinimumSize.x,MaxX);
		int ZSize = (int)Random.Range(MinimumSize.y,MaxZ);

	
		
		int[,,] WorkingSpace = InitWorkingSpace (-1, XSize, ZSize);
		byte[,,] RotationSpace = InitRotationSpace (Rotation.South	, XSize, ZSize);
		
		
		//Debug.Log(MinimumSize + " " + MaximumSize + " " + MaxWidth + " " + MaxHeight);
		
		//SetRectArea(0,0,0,(int)XSize,(int)ZSize,0,WorkingSpace, World);

		SetRectOutline(0,0,0,(int)XSize,(int)ZSize,17,WorkingSpace, World);
		SetRectArea(1,1,1,(int)XSize-1,(int)ZSize-1,14,WorkingSpace, World);
		//SetRectOutline(2,0,2,(int)XSize-3,(int)ZSize-3,15,WorkingSpace, World);
		//SetRectArea(1,2,1,(int)XSize-1,(int)ZSize-1,18, WorkingSpace,World); // fill empty space
		//SetRectArea(1,3,1,(int)XSize-1,(int)ZSize-1,18,WorkingSpace, World); // fill empty space
		
		SetRectOutlineWithWallFacing(1,2,1,(int)XSize-2,(int)ZSize-2,4,WorkingSpace,RotationSpace, World);
		SetRectOutlineWithWallFacing(1,3,1,(int)XSize-2,(int)ZSize-2,4,WorkingSpace,RotationSpace, World);

		//SetRectOutline(0,0,0,(int)XSize,(int)ZSize,17,WorkingSpace, World);

		SetXLine(false,2,XSize-2,2,1,6,WorkingSpace,World); //windows
		SetRectArea(2,2,2,(int)XSize-3,(int)ZSize-3,18, WorkingSpace,World); // fill empty space
		SetRectArea(2,3,2,(int)XSize-3,(int)ZSize-3,18,WorkingSpace, World);
		SetRectArea(0,4,0,(int)XSize,(int)ZSize,3,WorkingSpace, World); //roof
		
		//Entrance
		
		int XEntrance = (int)Mathf.Floor((XSize/2.0f));
		SetXLine(false,XEntrance-1,XEntrance+1,2,1,5,WorkingSpace,World); 
		
		// place shelves
		
		GenerateShelves (XEntrance,XSize,ZSize,WorkingSpace,RotationSpace,Front,World);
		
		// place counter
		
		// pick a corner, left or right...
		
		bool Left = Random.Range(0,100)>50;



		//if(Left)
		//{
		SetXLine(false,2,XEntrance,2,ZSize-4,15,WorkingSpace,World);
		SetZLine(false,ZSize-4,ZSize-2,2,XEntrance,15,WorkingSpace,World); 
		SetXLineRotation(false,2,XEntrance,2,ZSize-4,Rotation.South,RotationSpace,World);
		//}
		//else
		//{
			//SetXLine(x+XSize-6,x+XSize-2,2,z+ZSize-3,15,World);
		//}
		
		// place counter entrance
		
		Vector2 CounterPos = RandPointOnZLine(ZSize-1,ZSize,2+4);
		SetBlock((int)CounterPos.x,2,(int)CounterPos.y,19,WorkingSpace,World);
		
		// Include a back entrace?
		
		bool BackEntrance = Random.Range(0,100) > 50;
		
		if(BackEntrance)
		{
		
		}
		
		Stamp(x,z,XSize,ZSize,Front,WorkingSpace,RotationSpace,World);
		
		
		
	}
}
