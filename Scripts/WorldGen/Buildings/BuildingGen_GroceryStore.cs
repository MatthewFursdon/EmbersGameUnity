﻿
using UnityEngine;
using System.Collections;
	
public class BuildingGen_GroceryStore : BuildingGen_Shop
{
	
	
	public override void GenerateShelves(int XEntrance,int XSize,int ZSize,int[,,] WorkingSpace,byte[,,] RotationSpace,Rotation Front,GenerateWorld World)
	{
		
		int ShelfRows = (int)Mathf.Floor((ZSize - 5.0f));
		int shelfi = 0;
		
		while(shelfi < ShelfRows)
		{
			SetXLine(false,2,XSize-2,2,2+(shelfi),16,WorkingSpace,World);

		
			//SetXLineRotation(false,2,XSize-2,2,2+(shelfi),Front,RotationSpace,World);
		

			shelfi+=2;
		}
		shelfi-=2;
		SetZLine(false,2,2+(shelfi)+1,2,XEntrance-1,18,WorkingSpace,World); 
		SetZLine(false,2,2+(shelfi)+1,2,XEntrance,18,WorkingSpace,World); 
		
	}
}
