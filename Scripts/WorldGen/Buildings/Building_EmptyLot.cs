﻿using UnityEngine;
using System.Collections;

public class Building_EmptyLot : BuildingGen
{
	
	public override void Place(int x,int z,int MaxWidth,int MaxHeight,Rotation Front,GenerateWorld World)
	{


			int MaxX = (int)Mathf.Min(MaxWidth,MaximumSize.x);
			int MaxZ = (int)Mathf.Min(MaxHeight,MaximumSize.y);
			
			int XSize = (int)Random.Range(MinimumSize.x,MaxX);
			int ZSize = (int)Random.Range(MinimumSize.y,MaxZ);
			
			int[,,] WorkingSpace = new int[XSize,5,ZSize];
			byte[,,] RotationSpace = InitRotationSpace (Rotation.South, XSize, ZSize);
		
		SetRectArea(0,0,0,(int)XSize,(int)ZSize,23,WorkingSpace, World);
		SetRectOutline(0,1,0,(int)XSize,(int)ZSize,22,WorkingSpace, World);
		SetRectOutline(0,0,0,(int)XSize,(int)ZSize,1,WorkingSpace, World);

		SetXLine(false,1,XSize-1,1,0,0,WorkingSpace,World); 
	
		int i = 0;

		while (i < XSize*ZSize) {

			Vector2 V = RandPointInRectArea(0,0,XSize,ZSize);

			if(Random.Range(0,1000) <= 20)
			{
				//SetBlock((int)V.x,1,(int)V.y,24,WorkingSpace,World);
			}

			i++;
		}

		
		Stamp(x,z,XSize,ZSize,Front,WorkingSpace,RotationSpace,World);



		
		
	}
}
