﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingGen : MonoBehaviour
 {

	public Vector2 MinimumSize = new Vector2(9,9);
	public Vector2 MaximumSize = new Vector2(15,15);

	public enum Rotation{North=(byte)1,East=(byte)2,West=(byte)3,South=(byte)0};

	public static byte[,,] InitRotationSpace(Rotation defaultR,int XSize,int ZSize)
	{
		byte[,,] InitRotationSpace = new byte[XSize,Chunk.Z_LEVELS,ZSize];
		
		
		int x = 0;
		int y = 0;
		int z = 0;
		
		while(x < XSize)
		{
			while(z < ZSize)
			{
				while(y < Chunk.Z_LEVELS)
				{
					InitRotationSpace[x,y,z] = (byte)defaultR;
					y++;
				}
				y = 0;
				z++;
				
			}
			z=0;
			x++;
		}
		return InitRotationSpace;
		
	}

	public static int[,,] InitWorkingSpace(int defaultV,int XSize,int ZSize)
	{
		int[,,] WorkingSpace = new int[XSize,5,ZSize];


		int x = 0;
		int y = 0;
		int z = 0;

		while(x < XSize)
		{
			while(z < ZSize)
			{
				while(y < 5)
				{
					WorkingSpace[x,y,z] = defaultV;
					y++;
				}
				y = 0;
				z++;

			}
			z=0;
			x++;
		}
		return WorkingSpace;

	}


	public static Vector2 FindBuildableArea(int x,int y,int z,Rotation Dir,GenerateWorld world)
	{
	int X = x;
	int Z = z;


	if(Dir == Rotation.South || Dir == Rotation.West)
	{
		while(RectAreaBuildable(x,0,z,X-x,Z-z,world)&&RectAreaBuildable(x,1,z,X-x,Z-z,world))
		{
			X++;
			Z++;
			//Debug.Log("X:Z" + X + " " + Z);
			if(X-x > 99 || Z-z > 99)
			{
				break;
			}
		
		}
		
		while(RectAreaBuildable(x,0,z,X-x,Z-z,world)&&RectAreaBuildable(x,1,z,X-x,Z-z,world))
		{
			X++;
		
			if(X-x > 99 || Z-z > 99)
			{
				break;
			}
		}
		
		while(RectAreaBuildable(x,0,z,X-x,Z-z,world)&&RectAreaBuildable(x,1,z,X-x,Z-z,world))
		{
			Z++;
		
			if(X-x > 99 || Z-z > 99)
			{
				break;
			}
		}
		
		return new Vector2(X-x,Z-z);
	}
	else if(Dir == Rotation.North)
	{

		while(RectAreaBuildable(x,0,Z,X-x,z-Z,world)&&RectAreaBuildable(x,1,Z,X-x,z-Z,world))
		{
			X++;
			Z--;
			//Debug.Log("X:Z" + X + " " + Z);
			if(X-x > 99 || z-Z > 99)
			{
				break;
			}
		
		} ;

		while(RectAreaBuildable(x,0,Z,X-x,z-Z,world)&&RectAreaBuildable(x,1,Z,X-x,z-Z,world)) 
		{
			X++;
		
			if(X-x > 99 || z-Z > 99)
			{
				break;
			}
		} 
		
		while(RectAreaBuildable(x,0,Z,X-x,z-Z,world)&&RectAreaBuildable(x,1,Z,X-x,z-Z,world)) 
		{
			Z--;
		
			if(X-x > 99 || z-Z > 99)
			{
				break;
			}
		} 
		
		return new Vector2(X-x,z-Z);

	}
	else if(Dir == Rotation.East)
	{
			while(RectAreaBuildable(X,0,z,x-X,Z-z,world)&&RectAreaBuildable(X,1,z,x-X,Z-z,world))
		{
			X--;
			Z++;
			//Debug.Log("X:Z" + X + " " + Z);
			if(X-x > 99 || z-Z > 99)
			{
				break;
			}
		
		}
		
			while(RectAreaBuildable(X,0,z,x-X,Z-z,world)&&RectAreaBuildable(X,1,z,x-X,Z-z,world))
		{
			X--;
		
			if(X-x > 99 || z-Z > 99)
			{
				break;
			}
		}
		
		while(RectAreaBuildable(X,0,z,x-X,Z-z,world)&&RectAreaBuildable(X,1,z,x-X,Z-z,world))
		{
			Z++;
		
			if(X-x > 99 || z-Z > 99)
			{
				break;
			}
		}
		
		return new Vector2(x-X,Z-z);
	}
	

	return new Vector2(x-X,Z-z);
		
		
	}
	
	public virtual void Place(int x,int z,int MaxWidth,int MaxHeight,Rotation Front,GenerateWorld World)
	{
	
	}

	public virtual void PostStamp(int x,int z,int Width,int Height,Rotation Front,int[,,] WorkingSpace,byte[,,] RotationSpace,GenerateWorld World)
	{

	}

	public Vector2 workingToWorld(int x,int z,Vector2 pos,int Width,int Height,Rotation Front)
	{
		Vector2 Mod = new Vector2 (pos.x, pos.y);

		
		if(Front == Rotation.North)
		{
			Mod.x = (Width) - pos.x;
			Mod.y = (Height) -  pos.y;
		}
		
		if(Front == Rotation.West)
		{
			Mod.y =  pos.x;
			Mod.x =  (Height) - pos.y;
		}
		
		if(Front == Rotation.East)
		{
			Mod.y =  (Width) - pos.x;
			Mod.x =  pos.y;
		}

		return Mod+new Vector2(x,z);

	}


	public virtual void Stamp(int x,int z,int Width,int Height,Rotation Front,int[,,] WorkingSpace,byte[,,] RotationSpace,GenerateWorld World)
	{

		int StampX = 0;
		int StampZ = 0;

		int InitialWorldX = x;
		int InitialWorldZ = z;

		int WorldX = InitialWorldX;
		int WorldZ = InitialWorldZ;
		int WorldY = 0;

		RotateContents(RotationSpace,Width,Height,Front);

		if (Front == Rotation.North) 
		{
			InitialWorldZ = WorldZ - Height;
		}
		else if (Front == Rotation.East) 
		{
			InitialWorldX = WorldX - Width;
		}

		
		if(!RectAreaBuildable(InitialWorldX,0,InitialWorldZ,Width,Height,World) || !RectAreaBuildable(InitialWorldX,1,InitialWorldZ,Width,Height,World))
		{
			
			Debug.Log("bad placement");
			return;
			
		}



		while (StampX < Width) 
		{
			while(StampZ < Height)
			{
				while(WorldY < 5)
				{
				
					// default is south facing
					 WorldX =  StampX;
					 WorldZ =  StampZ;
					
					if(Front == Rotation.North)
					{
						WorldX = (Width) - StampX;
						WorldZ = (Height) - StampZ;
						
					}
					
					if(Front == Rotation.East)
					{
						WorldZ =  StampX;
						WorldX =  (Height) - StampZ;
					}
					
					if(Front == Rotation.West)
					{
						WorldZ =  (Width) - StampX;
						WorldX =  StampZ;
					}

					if(WorkingSpace[StampX,WorldY,StampZ] != -1)
					{
						if(WorkingSpace[StampX,WorldY,StampZ] == 26)
						{
							int f = 1;
						}

						World.world		  [InitialWorldX+WorldX,WorldY,InitialWorldZ+WorldZ] = WorkingSpace [StampX,WorldY,StampZ];
						World.RotationData[InitialWorldX+WorldX,WorldY,InitialWorldZ+WorldZ] = RotationSpace[StampX,WorldY,StampZ];
					}
					WorldY++;
				}
				WorldY = 0;
				StampZ++;
			}
			StampZ = 0;
			StampX++;

		}


	}

	/*
	public void Stamp(int x,int z,int Width,int Height,Rotation Front,int[,,] WorkingSpace,GenerateWorld World)
	{
	
		//Debug.Log("Stamping at " + x + " " + z);
	
		// X and Z position in stamp
		int WorkingX = 0;
		int WorkingZ = 0;
		int y = 0;
		
		int Xplace = x;
		int ZPlace = z;
		
		if(Front == Rotation.North)
		{
			ZPlace = z-Height;

		}

		if(Front == Rotation.East)
		{
			Xplace = x-Width;

			
		}

		if(Front == Rotation.West)
		{

			
		}


		

		
		if(!RectAreaBuildable(Xplace,0,ZPlace,Width,Height,World) || !RectAreaBuildable(Xplace,1,ZPlace,Width,Height,World))
		{
		
			Debug.Log("bad placement");
			return;
		
		}


		int ZEnd = Height;
		int XEnd = Width;

		if (Front == Rotation.East || Front == Rotation.West) {
			ZEnd = Width;
			XEnd = Height;
		}

		while(WorkingZ < ZEnd)
		{
			while(WorkingX < XEnd)
			{
				while(y < 5)
				{

				
				// default is south facing
				int ModifiedX =  WorkingX;
				int ModifiedZ =  WorkingZ;
				
				if(Front == Rotation.North)
				{
					ModifiedX = (Width) - WorkingX -1;
					ModifiedZ = (Height) - WorkingZ -1;
				
				}
				
				if(Front == Rotation.West)
				{
					ModifiedZ =  WorkingX;
					ModifiedX =  (Height) - WorkingZ -1;
				}
				
				if(Front == Rotation.East)
				{
					ModifiedZ =  (Width) - WorkingX - 1;
					ModifiedX =  WorkingZ;
				}
				
					//Debug.Log(Xplace+WorkingX + " " + ZPlace+WorkingZ + " " + ModifiedX + " " + ModifiedZ);
				if(ModifiedX >= 0 && ModifiedZ >= 0)
				{
				try
				{
							if(WorkingSpace[ModifiedX,y,ModifiedZ] != -1)
							{
								World.world[Xplace+WorkingX,y,ZPlace+WorkingZ] = WorkingSpace[ModifiedX,y,ModifiedZ];
							}
				}
				catch(System.IndexOutOfRangeException E)
				{
							System.IndexOutOfRangeException G = E;
							int a  = 1;
				}
				}
				
				y++;
				}
			y = 0;
			WorkingX++;
			}
		WorkingX = 0;
		WorkingZ++;
		}

		PostStamp(x,z,Width,Height,Front,WorkingSpace,World);
	}
	*/
	public static void SetRectArea(int x,int y,int z,int width,int height,int type,int[,,] WorkingSpace,GenerateWorld World)
	{
			int XStart = x;
			int XEnd = x+width;
			
			int ZStart = z;
			int ZEnd = z+height;
			
			if(width < 0)
			{
				XStart = x+width;
				XEnd = x;
			}
			
			if(height < 0)
			{
				ZStart = z+height;
				ZEnd = z;
			}
			
			int X = XStart;
			int Z = ZStart;
			

		if(World.SquareOnMap(x,z,width,height,GenerateWorld.WorldSize*Chunk.CHUNK_SIZE))
		{
	
			while(X < XEnd-1)
			{
				while(Z < ZEnd-1)
				{
				//World.world[X,y,Z] = type;
				WorkingSpace[X,y,Z] = type;
				Z++;
				}
			X++;
			Z = ZStart;
			}
			
		}
	}

	public static void SetXLineRotation(bool RealWorld,int start,int end,int y,int z,Rotation rot,byte[,,] RotationSpace, GenerateWorld World)
	{
		int t = start;
		while(t < end)
		{
			if(RealWorld)
			{
				World.RotationData[t,y,z] = (byte)rot;
			}
			else
			{
				RotationSpace[t,y,z] = (byte)rot;
			}
			t++;
		}	
	}

	public static void SetXLine(bool RealWorld,int start,int end,int y,int z,int type,int[,,] WorkingSpace,GenerateWorld World)
	{
		int t = start;
		while(t < end)
		{
			if(RealWorld)
			{
			World.world[t,y,z] = type;
			}
			else
			{
			WorkingSpace[t,y,z] = type;
			}
			t++;
		}	
	}
	
	public static void SetZLine(bool RealWorld,int start,int end,int y,int x,int type,int[,,] WorkingSpace,GenerateWorld World)
	{
		int t = start;
		while(t < end)
		{
			if(RealWorld)
			{
				World.world[x,y,t] = type;
			}
			else
			{
				WorkingSpace[x,y,t] = type;
			}
			t++;
		}	
	}
	
	public static void SetBlock(int x,int y,int z,int type,int[,,] WorkingSpace,GenerateWorld World)
	{
			//World.world[x,y,z] = type;
			WorkingSpace[x,y,z] = type;
	}

	public static void SetRectOutlineWithWallFacing(int x,int y, int z,int width,int height,int type,int[,,] WorkingSpace,byte[,,] RotationSpace, GenerateWorld World)
	{
		/// Set an outline of tiles, with the rotation set to run along the edge
		int XStart = x;
		int XEnd = x+width;
		
		int ZStart = z;
		int ZEnd = z+height;
		
		if(width < 0)
		{
			XStart = x+width;
			XEnd = x+1;
		}
		
		if(height < 0)
		{
			ZStart = z+height;
			ZEnd = z+1;
		}
		
		int X = XStart;
		int Z = ZStart;
		
		
		if(World.SquareOnMap(x,z,width,height,GenerateWorld.WorldSize*Chunk.CHUNK_SIZE))
		{
			
			while(X < XEnd)
			{
				while(Z < ZEnd)
				{
					if(X == XStart)
					{
						WorkingSpace[X,y,Z] = type;
						RotationSpace[X,y,Z] = (byte)Rotation.East;
					}

					if(X == XEnd-1)
					{
						WorkingSpace[X,y,Z] = type;
						RotationSpace[X,y,Z] = (byte)Rotation.West;
					}

					if(Z == ZStart)
					{
						WorkingSpace[X,y,Z] = type;
						RotationSpace[X,y,Z] = (byte)Rotation.North;
					}

					if(Z ==  ZEnd-1)
					{
						WorkingSpace[X,y,Z] = type;
						RotationSpace[X,y,Z] = (byte)Rotation.South;
					}

					Z++;
				}
				X++;
				Z = ZStart;
			}
			
		}
	}
	                                                

	public static void SetRectOutline(int x,int y, int z,int width,int height,int type,int[,,] WorkingSpace,GenerateWorld World)
	{
			int XStart = x;
			int XEnd = x+width;
			
			int ZStart = z;
			int ZEnd = z+height;
			
			if(width < 0)
			{
				XStart = x+width;
				XEnd = x+1;
			}
			
			if(height < 0)
			{
				ZStart = z+height;
				ZEnd = z+1;
			}
			
			int X = XStart;
			int Z = ZStart;
			

		if(World.SquareOnMap(x,z,width,height,GenerateWorld.WorldSize*Chunk.CHUNK_SIZE))
		{
	
			while(X < XEnd)
			{
				while(Z < ZEnd)
				{
					if(X == XStart || X == XEnd-1 || Z == ZStart || Z == ZEnd-1)
					{
					//World.world[X,y,Z] = type;
					WorkingSpace[X,y,Z] = type;
					}
					Z++;
				}
			X++;
			Z = ZStart;
			}
			
		}
	}

	public void RotateContents(byte[,,] RotationSpace,int XSize,int ZSize,Rotation Front)
	{
		int x = 0;
		int y = 0;
		int z = 0;

		while (x < XSize) 
		{
			while(z < ZSize)
			{
				y = 0;
				while( y <  Chunk.Z_LEVELS)
				{
					Rotation Rot = (Rotation)(RotationSpace[x,y,z]);

					if(Front == Rotation.South)
					{
						//
						//	Don't do anything
						//
					}
					else if(Front == Rotation.East)
					{
						if(Rot == Rotation.North)
						{
							RotationSpace[x,y,z] = (byte)Rotation.West;
						}
						else if(Rot == Rotation.East)
						{
							RotationSpace[x,y,z] = (byte)Rotation.North;
						}
						else if(Rot == Rotation.South)
						{
							RotationSpace[x,y,z] = (byte)Rotation.East;
						}
						else if(Rot == Rotation.West)
						{
							RotationSpace[x,y,z] = (byte)Rotation.South;
						}
					}
					else if(Front == Rotation.North)
					{
						if(Rot == Rotation.North)
						{
							RotationSpace[x,y,z] = (byte)Rotation.South;
						}
						else if(Rot == Rotation.East)
						{
							RotationSpace[x,y,z] = (byte)Rotation.West;
						}
						else if(Rot == Rotation.South)
						{
							RotationSpace[x,y,z] = (byte)Rotation.North;
						}
						else if(Rot == Rotation.West)
						{
							RotationSpace[x,y,z] = (byte)Rotation.East;
						}
					}
					else if(Front == Rotation.West)
					{
						if(Rot == Rotation.North)
						{
							RotationSpace[x,y,z] = (byte)Rotation.East;
						}
						else if(Rot == Rotation.East)
						{
							RotationSpace[x,y,z] = (byte)Rotation.South;
						}
						else if(Rot == Rotation.South)
						{
							RotationSpace[x,y,z] = (byte)Rotation.West;
						}
						else if(Rot == Rotation.West)
						{
							RotationSpace[x,y,z] = (byte)Rotation.North;
						}
					}

					y++;
				}
				z++;
			}
			x++;
			z=0;
		}

	}
	public static Vector2 RandPointOnXLine(int start, int end,int z)
	{
		return new Vector2(Random.Range(start,end),z);
	
	}
	
	public static Vector2 RandPointOnZLine(int start, int end,int x)
	{
		return new Vector2(x,Random.Range(start,end));
	
	}

	public static Vector3 RandPointOnRectOutlineWithOrientation(int x,int z,int width,int height,int BlocksOutFromCorner=0)
	{
		
		bool Horizontal = Random.Range(0,100) > 50;
		bool TopRight = Random.Range(0,100) > 50;
		
		int X = -1;
		int Z = -1;
		Rotation ori;
		if(Horizontal)
		{
			if(TopRight)
			{
				Z = Z+height-1;
				ori = Rotation.South;
			}
			else
			{
				Z = z;
				ori = Rotation.North;
			}
			
			X = Random.Range(x+BlocksOutFromCorner,x+width-1-BlocksOutFromCorner);
		}
		else
		{
			if(TopRight)
			{
				X = X+width-1;
				ori = Rotation.West;
			}
			else
			{
				X = x;
				ori = Rotation.East;
			}
			
			Z = Random.Range(z+BlocksOutFromCorner,z+height-1-BlocksOutFromCorner);
			
		}
		return new Vector3(X,Z,(float)((byte)ori));
	}


	public static Vector2 RandPointOnRectOutline(int x,int z,int width,int height)
	{
	
	bool Horizontal = Random.Range(0,100) > 50;
	bool TopRight = Random.Range(0,100) > 50;
	
	int X = -1;
	int Z = -1;
	if(Horizontal)
	{
		if(TopRight)
		{
		Z = Z+height-1;
		}
		else
		{
		Z = z;
		}
		
		X = Random.Range(x,x+width-1);
	}
	else
	{
		if(TopRight)
		{
		X = X+width-1;
		}
		else
		{
		X = x;
		}
		
		Z = Random.Range(z,z+height-1);
	
	}
	
	return new Vector2(X,Z);
	}
	
	public static Vector2 RandPointInRectArea(int x,int z,int width,int height)
	{

			int X = Random.Range (x, x + width);
			int Z = Random.Range (z, z + height);
			return new Vector2(X,Z);

	}
	
	public static bool RectAreaBuildable(int x,int y,int z,int width,int height,GenerateWorld world)
	{
	int X = x;
	int Z = z;
	
	while(X <= x+width)
	{
		while(Z <= z+height)
		{
		if(!IsBuildable(X,y,Z,world))
		{
		return false;
		}
		Z++;
		}
	X++;
	Z = z;
	}
	
	return true;
	}
	
	public static bool RectOutlineBuildable(int x,int z,int width,int height)
	{
	return false;
	
	}
	public static bool IsBuildable(int x,int y,int z,GenerateWorld world)
	{
		if(World.PositionIsValid(x,z))
		{
			int a = world.world[x,y,z];
			//return( a != 8 && a != 9 && a != 10 && a != 11 && a != 12 && a != 13 && a != 17 && a != 4 && a != 5 && a != 6 && a != 0);
			return(a==1 || a == 0);
			//return true;
		}
		return false;
	}
	
}
