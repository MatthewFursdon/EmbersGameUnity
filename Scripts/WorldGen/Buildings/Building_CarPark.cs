﻿using UnityEngine;
using System.Collections;

public class Building_CarPark : BuildingGen
{



	public override void Place(int x,int z,int MaxWidth,int MaxHeight,Rotation Front,GenerateWorld World)
	{
		
		
		int MaxX = (int)Mathf.Min(MaxWidth,MaximumSize.x);
		int MaxZ = (int)Mathf.Min(MaxHeight,MaximumSize.y);
		
		int XSize = (int)Random.Range(MinimumSize.x,MaxX);
		int ZSize = (int)Random.Range(MinimumSize.y,MaxZ);
		
		int[,,] WorkingSpace = InitWorkingSpace (-1,XSize, ZSize);
		byte[,,] RotationSpace = InitRotationSpace (Rotation.South, XSize, ZSize);

		if (Front == Rotation.North || Front == Rotation.South) {
		
			SetRectArea (0, 0, 0, (int)XSize, (int)ZSize, 12, WorkingSpace, World);
		}
		else
		{
			SetRectArea (0, 0, 0, (int)XSize, (int)ZSize, 9, WorkingSpace, World);
		}
		SetRectOutline(0,1,0,(int)XSize,(int)ZSize,22,WorkingSpace, World);

		
		Stamp(x,z,XSize,ZSize,Front,WorkingSpace,RotationSpace,World);
		
		
		
		
		
	}
}
