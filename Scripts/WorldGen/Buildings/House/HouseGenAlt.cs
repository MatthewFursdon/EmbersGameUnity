﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HouseGenAlt : BuildingGen
{
	

	public class HouseRoom
	{
		public int X;
		public int Z;
		public int Width;
		public int Depth;
		
		Vector2 DoorPosition;
		
		public HouseRoom(int x,int z,int width,int depth,Vector2 doorp)
		{
			X = x;
			Z = z;
			Width = width;
			Depth = depth;
		}

	}

	public List<HouseRoomGenerator> RoomGenerators;
	int[,,] WorkingSpace;
	byte[,,] RotationSpace;
	public List<HouseRoomGenerator> FindUsableGenerators(int XSize, int ZSize,BuildingGen.Rotation Front,List<HouseRoomGenerator> bg)
	{
		Vector2 space = new Vector2 (XSize, ZSize);
		List<HouseRoomGenerator> Usable = new List<HouseRoomGenerator>();
		foreach(HouseRoomGenerator B in bg)
		{
			Vector2 dif = B.MinimumSize - space;
			//Debug.Log( B.MinimumSize + " " + space + " " + dif);
			if(dif.x < 0 && dif.y < 0)
			{
				//usable
				Usable.Add(B);
			}
		}	
		
		return Usable;
		
	}
	
	public void PlaceRandomRoom(int x,int z,int XSize, int ZSize,BuildingGen.Rotation Front,List<HouseRoomGenerator> bg,GenerateWorld World)
	{
		Vector2 space = new Vector2 (XSize, ZSize);
		List<HouseRoomGenerator> buildings = FindUsableGenerators(XSize,ZSize,Front,bg);
		int i = Random.Range(0,buildings.Count-1);
		
		if(buildings.Count > 0)
		{

			buildings[i].Place(x,z,(int)space.x,(int)space.y,Front,World,WorkingSpace,RotationSpace);
		}
	}

	public override void Place(int x,int z,int MaxWidth,int MaxHeight,Rotation Front,GenerateWorld World)
	{
		List<HouseRoom> Rooms = new List<HouseRoom> ();
		int MaxX = (int)Mathf.Min(MaxWidth,MaximumSize.x);
		int MaxZ = (int)Mathf.Min(MaxHeight,MaximumSize.y);
		
		int XSize = (int)Random.Range(MinimumSize.x,MaxX); // lot size
		int ZSize = (int)Random.Range(MinimumSize.y,MaxZ); 

		int HallX = (int)Mathf.Round(0.5f * XSize);
		int HallZ = (int)Mathf.Round(0.1f * ZSize);

		int HallLength = (int)Mathf.Round(0.6f * ZSize);

		if (HallLength >= 20) {
			HallLength = 20;
		}

		WorkingSpace = InitWorkingSpace (-1, XSize, ZSize);
		RotationSpace = InitRotationSpace (Rotation.South, XSize, ZSize);

		SetRectOutline(0,1,0,(int)XSize,(int)ZSize,22,WorkingSpace, World); // lot fence

		// back garden

		if (Random.Range (0, 100) < 20) {
			SetRectArea (1, 0, ZSize - 3, XSize - 1, 3, 24, WorkingSpace, World);
		}

		// main hall
		SetRectArea(HallX-1,1,HallZ,3,(int)HallLength,14,WorkingSpace, World);

		SetZLine (false, HallZ + 1, HallZ + HallLength - 1, 2, HallX, 18, WorkingSpace, World);
		SetZLine (false, HallZ + 1, HallZ + HallLength - 1, 3, HallX, 18, WorkingSpace, World);

		SetRectArea(HallX-1,4,HallZ,3,(int)HallLength,3,WorkingSpace, World);

		int leftrooms = HallLength+Random.Range(-3,3);
		int rightrooms = HallLength+Random.Range(-3,3);

		int last = 0;

		// rooms to the left
		while (leftrooms > 0) 
		{
			int room = Random.Range(3,6);
			if(room > leftrooms)
			{
				room = leftrooms;
			}


			int roomdepth = Random.Range(3,5);

			int start = (int)Mathf.Round(HallZ + last);
			SetRectOutline(HallX+1,2,start,roomdepth,room,4,WorkingSpace, World);
			SetRectOutline(HallX+1,3,start,roomdepth,room,4,WorkingSpace, World);

			// fill empty space
			SetRectArea(HallX+2,2,start+1,roomdepth-1,room-1,18,WorkingSpace, World);
			SetRectArea(HallX+2,3,start+1,roomdepth-1,room-1,18,WorkingSpace, World);

			SetRectArea(HallX+1,1,start,roomdepth+1,room+1,14,WorkingSpace, World);
			SetRectArea(HallX+1,4,start,roomdepth+1,room+1,3,WorkingSpace, World);

			Vector2 DoorPosition = RandPointOnZLine(start+1,start+room-1,HallX+1);
			SetBlock((int)DoorPosition.x,2,(int)DoorPosition.y,5,WorkingSpace,World);
			HouseRoom R = new HouseRoom(HallX+1,start,roomdepth+1,room+1,DoorPosition);
			Rooms.Add(R);

			last += room-1;
			leftrooms-= room;

			PlaceRandomRoom(R.X,R.Z,R.Width,R.Depth,Rotation.South,RoomGenerators,World);


		}




		// rooms to the right
		last = 0;
		while (rightrooms > 0) 
		{
			int room = Random.Range(3,6);
			if(room > rightrooms)
			{
				room = rightrooms;
			}
			
			
			int roomdepth = Random.Range(3,5);
			
			int start = (int)Mathf.Round(HallZ + last );
			SetRectOutline(HallX-1,2,start,-roomdepth,room,4,WorkingSpace, World);
			SetRectOutline(HallX-1,3,start,-roomdepth,room,4,WorkingSpace, World);

			// fill empty space
			SetRectArea(HallX,2,start+1,-roomdepth,room,18,WorkingSpace, World);
			SetRectArea(HallX,3,start+1,-roomdepth,room,18,WorkingSpace, World);

			SetRectArea(HallX,1,start,-roomdepth-1,room+1,14,WorkingSpace, World);
			SetRectArea(HallX,4,start,-roomdepth-1,room+1,3,WorkingSpace, World);

			Vector2 DoorPosition = RandPointOnZLine(start+1,start+room-1,HallX-1);
			SetBlock((int)DoorPosition.x,2,(int)DoorPosition.y,5,WorkingSpace,World);
			HouseRoom R = new HouseRoom(HallX-1,start,roomdepth,room,DoorPosition);
			Rooms.Add(R);
			
			last += room -1;
			rightrooms-= room;

			//PlaceRandomRoom(R.X,R.Z,R.Width,R.Depth,Rotation.West,RoomGenerators,World);
		}

		// cap hall
		SetXLine (false, HallX - 1, HallX + 3, 2, HallZ + HallLength-2, 4, WorkingSpace, World);
		SetXLine (false, HallX - 1, HallX + 3, 2, HallZ, 4, WorkingSpace, World);

		SetXLine (false, HallX - 1, HallX + 3, 3, HallZ + HallLength-2, 4, WorkingSpace, World);
		SetXLine (false, HallX - 1, HallX + 3, 3, HallZ, 4, WorkingSpace, World);

		// install door
		SetBlock (HallX, 2, HallZ, 5, WorkingSpace, World);
		SetXLine (false,HallX-3,HallX+3,1,0,0,WorkingSpace,World);

		int i = 0;
		while (i < XSize*ZSize) {
			
			Vector2 V = RandPointInRectArea(0,0,XSize,ZSize);
			
			if((WorkingSpace[(int)V.x,1,(int)V.y] == 0 || WorkingSpace[(int)V.x,1,(int)V.y] == -1) && Random.Range(0,1000) <= 20)
			{
				SetBlock((int)V.x,0,(int)V.y,25,WorkingSpace,World);
			}
			
			i++;
		}

		
		Stamp(x,z,XSize,ZSize,Front,WorkingSpace,RotationSpace,World);
		
		
		
	}
}