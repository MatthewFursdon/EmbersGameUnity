﻿using UnityEngine;
using System.Collections;

public class BedroomGenerator : HouseRoomGenerator
{
	public override void Place(int X,int Z,int XSize,int ZSize,Rotation Front,GenerateWorld World,int[,,] ParentWorkingSpace,byte[,,] ParentRotationSpace)
	{
		
		
//		Debug.Log("sdfsdfsdfsdf");
		int[,,] WorkingSpace = InitWorkingSpace (-1,XSize,ZSize);
		byte[,,] RotationSpace = InitRotationSpace(Rotation.South,XSize,ZSize);
		Vector2 BedPos = RandPointInRectArea (1, 1, XSize-1, ZSize-1);


		SetBlock ((int)BedPos.x, 2, (int)BedPos.y, 26, WorkingSpace, World);
		//SetBlock (1, 2,0, 6, WorkingSpace, World);
		//SetBlock (2, 2,0, 6, WorkingSpace, World);

		Stamp(X,Z,XSize,ZSize,Front,WorkingSpace,RotationSpace,World,ParentWorkingSpace,ParentRotationSpace);
		
		
		
		
		
	}


}
