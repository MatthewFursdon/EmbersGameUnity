﻿using UnityEngine;
using System.Collections;

public class HouseRoomGenerator : BuildingGen
{
	

	
	public virtual void Place(int X,int Z,int XSize,int ZSize,Rotation Front,GenerateWorld World,int[,,] ParentWorkingSpace,byte[,,] ParentRotationSpace)
	{
		

	
		
		
		
		
	}


	// don't stamp to the world, stamp to parent working space.
	public virtual void Stamp(int x,int z,int Width,int Height,Rotation Front,int[,,] WorkingSpace,byte[,,] RotationSpace,GenerateWorld World,int[,,] ParentWorkingSpace,byte[,,] ParentRotationSpace)
	{
		
		int StampX = 0;
		int StampZ = 0;
		
		int InitialWorldX = x;
		int InitialWorldZ = z;
		
		int WorldX = InitialWorldX;
		int WorldZ = InitialWorldZ;
		int WorldY = 0;
		
		if (Front == Rotation.North) 
		{
			InitialWorldZ = WorldZ - Height;
		}
		else if (Front == Rotation.East) 
		{
			InitialWorldX = WorldX - Width;
		}
	
		RotateContents (RotationSpace, Width, Height, Front);
		
		
		while (StampX < Width) 
		{
			while(StampZ < Height)
			{
				while(WorldY < 5)
				{
					
					// default is south facing
					WorldX =  StampX;
					WorldZ =  StampZ;
					
					if(Front == Rotation.North)
					{
						WorldX = (Width) - StampX;
						WorldZ = (Height) - StampZ;
						
					}
					
					if(Front == Rotation.East)
					{
						WorldZ =  StampX;
						WorldX =  (Height) - StampZ;
					}
					
					if(Front == Rotation.West)
					{
						WorldZ =  (Width) - StampX;
						WorldX =  StampZ;
					}
					
					if(WorkingSpace[StampX,WorldY,StampZ] != -1)
					{
						ParentWorkingSpace[InitialWorldX+WorldX,WorldY,InitialWorldZ+WorldZ] = WorkingSpace[StampX,WorldY,StampZ];
						ParentRotationSpace[InitialWorldX+WorldX,WorldY,InitialWorldZ+WorldZ] = RotationSpace[StampX,WorldY,StampZ];
					}
					WorldY++;
				}
				WorldY = 0;
				StampZ++;
			}
			StampZ = 0;
			StampX++;
			
		}
		
		
	}
}
