﻿using UnityEngine;
using System.Collections;

public class BathroomGen: HouseRoomGenerator
{
	public override void Place(int X,int Z,int XSize,int ZSize,Rotation Front,GenerateWorld World,int[,,] ParentWorkingSpace,byte[,,] ParentRotationSpace)
	{
		
		
	
		int[,,] WorkingSpace = InitWorkingSpace (-1,XSize,ZSize);
		byte[,,] RotationSpace = InitRotationSpace(Rotation.South,XSize,ZSize);
	
		//SetBlock (1, 2,0, 6, WorkingSpace, World);
		//SetBlock (2, 2,0, 6, WorkingSpace, World);
		
		Vector3 ToiletPosition = RandPointOnRectOutlineWithOrientation (0, 0, XSize, ZSize,1);
		BuildingGen.Rotation ToiletOrientation = (BuildingGen.Rotation)((byte)ToiletPosition.z);
		SetBlock ((int)ToiletPosition.x, 2,(int)ToiletPosition.y, 27, WorkingSpace, World);
		RotationSpace [(int)ToiletPosition.x, 2, (int)ToiletPosition.y] = (byte)ToiletOrientation;

		SetRectArea (0, 1,0, XSize, ZSize, 28, WorkingSpace, World);

		//Vector3 WindowPosition = RandPointOnRectOutlineWithOrientation (0, 0, XSize, ZSize,1);
		//SetBlock ((int)WindowPosition.x, 2,(int)WindowPosition.y, 6, WorkingSpace, World);

		Stamp(X,Z,XSize,ZSize,Front,WorkingSpace,RotationSpace,World,ParentWorkingSpace,ParentRotationSpace);
		
		
		
		
		
	}
	
	
}
