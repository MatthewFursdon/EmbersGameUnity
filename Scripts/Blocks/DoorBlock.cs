﻿using UnityEngine;
using System.Collections;

public class DoorBlock : Block {

	public GameObject Hor;
	public GameObject Vert;



	public override void MapLoaded()
	{
		transform.rotation = Chunk.South;
		Vector3 VU = new Vector3(Position.x,Position.y,Position.z+1);
		Vector3 VD = new Vector3(Position.x,Position.y,Position.z-1);
		Vector3 VL = new Vector3(Position.x-1,Position.y,Position.z);
		Vector3 VR = new Vector3(Position.x+1,Position.y,Position.z);

		Block U = World.GetBlock(VU);
		Block D = World.GetBlock(VD);
		Block L = World.GetBlock(VL);
		Block R = World.GetBlock(VR);


		Vert.SetActive(L != null && R != null && L.IsType(Block.BLOCK_TYPE.BLOCK_WALL) && R.IsType(Block.BLOCK_TYPE.BLOCK_WALL));
		Hor.SetActive(U != null && D != null && U.IsType(Block.BLOCK_TYPE.BLOCK_WALL) && D.IsType(Block.BLOCK_TYPE.BLOCK_WALL));

	}
}
