﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class JunkGenerator : MonoBehaviour {


public List<GameObject> Junk = new List<GameObject>();
public int MinJunk = 0;
public int MaxJunk = 0;
public int JunkChance = 10;
	// Use this for initialization
	void Awake () 
	{
	if(Random.Range(0,100) < JunkChance)
	{
	int JunkCount = Random.Range(MinJunk,MaxJunk);
	int i = 0;
	while(i < JunkCount)
	{
		GameObject JunkObject = ((GameObject)(GameObject.Instantiate(Junk[Random.Range(0,Junk.Count)],Vector3.zero,Quaternion.identity)));
		JunkObject.transform.parent = transform;
		JunkObject.transform.localPosition = new Vector3(Random.Range(0,1.0f),0.5f,Random.Range(0,1.0f));
	i++;
	}
	}
	}
	


}
