﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
public class Chunk : MonoBehaviour {

	public const int CHUNK_SIZE = 5;
	public const int Z_LEVELS = 5;
	public Vector2 ChunkPosition;
	public Collider ViewCollider;
	public List<GameObject> BlockTypes;
	public int HideTime = 10;
	public Block[, ,] Blocks;
	public int[, ,] BlockData = new int[Chunk.CHUNK_SIZE,Chunk.Z_LEVELS,Chunk.CHUNK_SIZE];
	public byte[, ,] RotationData = new byte[Chunk.CHUNK_SIZE,Chunk.Z_LEVELS,Chunk.CHUNK_SIZE];

	public bool Visible = false;
	private bool visibletracker = true;

	public static Quaternion North;
	public static Quaternion East;
	public static Quaternion South;
	public static Quaternion West;

	// Use this for initialization
	public Renderer[] Renderers;
	public void CreateBlocks () 
	{
		//new chunk. initizize blocks (1st z for now)
		Blocks = new Block[Chunk.CHUNK_SIZE,Chunk.Z_LEVELS,Chunk.CHUNK_SIZE];

		int x  = 0;
		int z  = 0;
		int y = 0;

		//
		// Place the 2nd Z-level directly over the 1st, this allows for eg, placing trees on grass
		//

		while(y < Chunk.Z_LEVELS)
		{
		
		while(x < Chunk.CHUNK_SIZE)
		{
			while(z < Chunk.CHUNK_SIZE)
			{
				Vector3 pos = new  Vector3(x*Block.BLOCK_SIZE,0,z*Block.BLOCK_SIZE);

				if(y>0 && BlockData[x,y-1,z] != 0)
				{
				//Debug.Log(x + " " + y + " " + z);
				pos.y = Blocks[x,y-1,z].transform.position.y+Blocks[x,y-1,z].height;
				}
				
				int i = Random.Range(0,BlockTypes.Count);

				if(z == 7)
				{
					i = 10;
				}
				else
				{
					if(i==10)
					{
						i  = 6;
					}
				}

				if(BlockData[x,y,z] != 0)
				{
				Blocks[x,y,z] = ((GameObject)(GameObject.Instantiate(BlockTypes[BlockData[x,y,z]],Vector3.zero,Quaternion.identity))).GetComponent<Block>();
				Blocks[x,y,z].transform.parent = transform;
				Blocks[x,y,z].transform.localPosition = pos;
				Blocks[x,y,z].transform.rotation = RotFromConst( RotationData[x,y,z]);
			
						if(Blocks[x,y,z].transform.rotation != South)
						{
							int a = 1;
						}

				Blocks[x,y,z].Position = new Vector3(x+ChunkPosition.x*CHUNK_SIZE,y,z+ChunkPosition.y*CHUNK_SIZE);
				}
				z++;
			}
			z=0;
			x++;
		}
		x = 0;
		z = 0;
		y++;
		}

	
	
	}

	Quaternion RotFromConst(byte BR)
	{
		BuildingGen.Rotation R = (BuildingGen.Rotation)BR;

		if (R == BuildingGen.Rotation.North) {
			return North;
		}
		else if (R == BuildingGen.Rotation.East) {
			return East;
		}
		else if (R == BuildingGen.Rotation.South) {
			return South;
		}
		else {
			return West;
		}
	}

	public void UpdateRenderStatus()
	{
		HideTime--;
		
		if(Visible)
		{
			ViewCollider.enabled = false;
		}
		else
		{
			ViewCollider.enabled = true;
		}
		
		if(Visible && HideTime < 0)
		{
			Visible = false;
		}
	
		if(Visible != visibletracker)
		{
			foreach(Renderer r in Renderers)
			{
				if(r != null)
				{
				r.enabled = (Visible);
				}
			}
			visibletracker = Visible;
		}
	}

	 private void FitColliderToChildren (GameObject parentObject)
   {
        BoxCollider bc = parentObject.GetComponent<BoxCollider>();
        if(bc==null)
		{
		bc = parentObject.AddComponent<BoxCollider>();
		}
        Bounds bounds = new Bounds (Vector3.zero, Vector3.zero);
        bool hasBounds = false;
        Renderer[] renderers =  parentObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer render in renderers) {
            if (hasBounds) {
                bounds.Encapsulate(render.bounds);
            } else {
                bounds = render.bounds;
                hasBounds = true;
           }
       }
      if (hasBounds) 
	  {
            bc.center = (bounds.center - parentObject.transform.position)-new Vector3(0f,3.0f,0.0f);
           bc.size = Vector3.Scale(bounds.size,new Vector3(1.0f,2.3f,1.0f));
			//bc.size = new Vector3(bc.size.x,20,bc.size.z);
      } else 
	  {
            bc.size = bc.center = Vector3.zero;
            bc.size = Vector3.zero;
      }
   }
 

	public void ReCheckNeighbors()
	{
		int  x = 0;
		int  z = 0;
		
		int y = 0;
		//Debug.Log("Running post-load");
		while(y < Chunk.Z_LEVELS)
		{
		
		while(x < Chunk.CHUNK_SIZE)
		{
			while(z < Chunk.CHUNK_SIZE)
			{
				if(Blocks[x,y,z] != null)
				{
				Blocks[x,y,z].MapLoaded();
				}
				z++;
			}
			z=0;
			x++;
		}
		y++;
		x=0;
		z=0;
	}
		//recalculate bounds
		
	
		//FitColliderToChildren(gameObject);
		Renderers = GetComponentsInChildren<Renderer>();	
	}
	// Update is called once per frame

	void Awake()
	{
		North = Quaternion.Euler (new Vector3 (0, -180, 0));
		East = Quaternion.Euler (new Vector3 (0, -90, 0));
		South = Quaternion.Euler (new Vector3 (0, 0, 0));
		West = Quaternion.Euler (new Vector3 (0, 90, 0));
	}

	void Start () 
	{


	InvokeRepeating("UpdateRenderStatus",1,0.1f);
	//FitColliderToChildren(this.gameObject);
	}
}
