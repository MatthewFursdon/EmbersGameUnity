﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WallBlock : Block {

	public GameObject Up;
	public GameObject Down;
	public GameObject Left;
	public GameObject Right;

	public BLOCK_TYPE MatchBlock = Block.BLOCK_TYPE.BLOCK_WALL;
	
	public override void MapLoaded()
	{

		//ignore rotation, for now.

		transform.rotation = Chunk.South;

		Vector3 VU = new Vector3(Position.x,Position.y,Position.z+1);
		Vector3 VD = new Vector3(Position.x,Position.y,Position.z-1);
		Vector3 VL = new Vector3(Position.x-1,Position.y,Position.z);
		Vector3 VR = new Vector3(Position.x+1,Position.y,Position.z);

		Block U = World.GetBlock(VU);
		Block D = World.GetBlock(VD);
		Block L = World.GetBlock(VL);
		Block R = World.GetBlock(VR);

		if (MatchBlock == null) {

			Up.SetActive (U != null && U.IsType (Block.BLOCK_TYPE.BLOCK_WALL));
			Down.SetActive (D != null && D.IsType (Block.BLOCK_TYPE.BLOCK_WALL));
			Left.SetActive (L != null && L.IsType (Block.BLOCK_TYPE.BLOCK_WALL));
			Right.SetActive (R != null && R.IsType (Block.BLOCK_TYPE.BLOCK_WALL));
		} else {
			Up.SetActive (U != null && U.IsType (Block.BLOCK_TYPE.BLOCK_WALL) && U.IsType (MatchBlock));
			Down.SetActive (D != null && D.IsType (Block.BLOCK_TYPE.BLOCK_WALL)&& D.IsType (MatchBlock));
			Left.SetActive (L != null && L.IsType (Block.BLOCK_TYPE.BLOCK_WALL)&& L.IsType (MatchBlock));
			Right.SetActive (R != null && R.IsType (Block.BLOCK_TYPE.BLOCK_WALL)&& R.IsType (MatchBlock));
		}

	}
}
