﻿using UnityEngine;
using System.Collections;

public class AlternateWallBlock : Block {
	
	public GameObject Right;
	public GameObject Down;
	public GameObject Middle;

	
	public BLOCK_TYPE MatchBlock = Block.BLOCK_TYPE.BLOCK_WALL;
	
	public override void MapLoaded()
	{
		


		//Vector3 VU = new Vector3(Position.x,Position.y,Position.z+1);
		Vector3 VD = new Vector3(Position.x,Position.y,Position.z-1);
		//Vector3 VL = new Vector3(Position.x-1,Position.y,Position.z);
		Vector3 VR = new Vector3(Position.x+1,Position.y,Position.z);


		if (transform.rotation == Chunk.North)
		{
			VD = new Vector3(Position.x,Position.y,Position.z+1);
		}
		else if (transform.rotation == Chunk.East)
		{
			VD = new Vector3(Position.x+1,Position.y,Position.z);
			VR = new Vector3(Position.x,Position.y,Position.z+1);
		}
		else if (transform.rotation == Chunk.West)
		{
			VD = new Vector3(Position.x+1,Position.y,Position.z);
			VR = new Vector3(Position.x,Position.y,Position.z+1);
		}
		
	
		
		//Block U = World.GetBlock(VU);
		Block D = World.GetBlock(VD);
		//Block L = World.GetBlock(VL);
		Block R = World.GetBlock(VR);
		
		if (MatchBlock == null) {
			

			Down.SetActive (D != null && D.IsType (Block.BLOCK_TYPE.BLOCK_WALL));
			Right.SetActive (R != null && R.IsType (Block.BLOCK_TYPE.BLOCK_WALL));
		} else {

			Down.SetActive (D != null && D.IsType (Block.BLOCK_TYPE.BLOCK_WALL)&& D.IsType (MatchBlock));
			Right.SetActive (R != null && R.IsType (Block.BLOCK_TYPE.BLOCK_WALL)&& R.IsType (MatchBlock));
		}
		
	}
}
