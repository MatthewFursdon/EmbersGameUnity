﻿using UnityEngine;
using System.Collections;

public class DoorSubBlock : MonoBehaviour 
{
	public enum State{opening, open,closing,closed}
	public State CurrentState = State.closed;
	Quaternion Open;
	Quaternion Closed;
	
	void Start()
	{
		Open = Quaternion.Euler(0,90,0);
		Closed = Quaternion.Euler(0,0,0);
	}
	
	void FixedUpdate()
	{
		
	
		if(CurrentState == State.opening)
		{
			transform.localRotation = Quaternion.Slerp(transform.localRotation,Open,Time.deltaTime);
			
			float angle = Quaternion.Angle(transform.localRotation,Open);
			
			if(angle < 2)
			{
				CurrentState = State.open;
			}
			
		}
		else if(CurrentState == State.closing)
		{
			transform.localRotation = Quaternion.Slerp(transform.localRotation,Closed,Time.deltaTime);
			
			float angle = Quaternion.Angle(transform.localRotation,Closed);
			
			if(angle < 2)
			{
				CurrentState = State.closed;
			}
			
		}
		
		
	}
	
	public void OpenOrClose()
	{
		if(CurrentState == State.closed)
		{
		CurrentState = State.opening;
		}
		
		
		if(CurrentState == State.open )
		{
		CurrentState = State.closing;
		}
	}
}
