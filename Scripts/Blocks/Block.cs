﻿using UnityEngine;
using System.Collections;

//
// Basic building block of the world
// door, wall etc.
//



public class Block : MonoBehaviour {

	public const int BLOCK_SIZE = 10;
	public bool Highest = true; //nothing can be built above this, eg a tree
	public string Description = "a blank description";
	public Vector3 Position;
	public int type;
	public float height;
	
	public enum BLOCK_TYPE {BLOCK_WILD,BLOCK_WALL,BLOCK_DOOR,BLOCK_FLOOR,BLOCK_FENCE,BLOCK_FURNITURE,BLOCK_SPECIAL,BLOCK_STRUCTURAL};
	public BLOCK_TYPE Type1;
	public BLOCK_TYPE Type2;
	public BLOCK_TYPE Type3;
	// Use this for initialization
	void Start () {
	
	}
	

	
	public bool IsType(BLOCK_TYPE t)
	{
	return(Type1 == t || Type2 == t || Type3 == t);
	}
	
	public bool IsType(Vector3 pos, BLOCK_TYPE t)
	{
	int BlockType = World.GetBlock(pos).type;
	Block B = (Block)GenerateWorld.ChunkDef.BlockTypes[BlockType].GetComponent<Block>();
	return B.IsType(t);
	//(Type1 == t || Type2 == t || Type3 == t);
	}

	//runs once you can be sure all the map has loaded. change based on your neighbors here
	public virtual void MapLoaded()
	{

	}
}
