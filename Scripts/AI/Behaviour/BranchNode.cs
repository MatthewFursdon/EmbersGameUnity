﻿using UnityEngine;
using System.Collections;

public class BranchNode  : BehaviourNode
{

	int SuccessNode;
	int FaliureNode;
	int PreviousNode;
	public BranchNode(int NID,int NNode,BehaviourTree owner,int PN,int SN,int FN) : base(NID,NNode,owner)
	{
		SuccessNode = SN;
		FaliureNode = FN;
		PreviousNode = PN;
	}
	
	public override void Run()
	{
			if(Owner.Nodes[PreviousNode].Status == status.Faliure)
			{
				NextNode = FaliureNode;
				Owner.MoveToNextNode();
				return;
			}
			else if(Owner.Nodes[PreviousNode].Status == status.Success)
			{
				NextNode = SuccessNode;
				Owner.MoveToNextNode();
				return;
			}
	}


}
