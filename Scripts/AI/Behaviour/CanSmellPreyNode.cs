﻿using UnityEngine;
using System.Collections;

public class CanSmellPreyNode  : BehaviourNode {

	public float InterestCutoff;

	public CanSmellPreyNode(int NID,int NNode,BehaviourTree owner,float ICO) : base(NID,NNode,owner)
	{
		InterestCutoff = ICO;
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		
		if(OwnerNPC != null)
		{
			if(OwnerNPC.CurrentSmell != null)
			{
				if(OwnerNPC.CurrentSmell.Interest >= InterestCutoff)
				{
					Status = status.Success;
				}
				else
				{
					Status = status.Faliure;
				}
			}
			else
			{
				Status = status.Faliure;
			}
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
