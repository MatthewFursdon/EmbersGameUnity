﻿using UnityEngine;
using System.Collections;

public class ConditionalSequenceNode  : BehaviourNode
{

	public int[] Sequence;
	int CurrentNodeInSequence = 0;
	int RealNextNode;
	public ConditionalSequenceNode(int NID,int NNode,BehaviourTree owner,int[] seq) : base(NID,NNode,owner)
	{
		Sequence = seq;
		NextNode = Sequence[0];
		RealNextNode = NNode;
	}
	
	public override void Run()
	{
		if(CurrentNodeInSequence < Sequence.Length && CurrentNodeInSequence > 0)
		{
			if(Owner.Nodes[Sequence[CurrentNodeInSequence-1]].Status == status.Faliure)
			{
				Debug.Log("node failed, ending sequence");
			
				Status = status.Faliure;
				NextNode = 0;
				CurrentNodeInSequence = 0;
				Owner.MoveToNextNode();
				return;
			}
		
		}
	
		if(CurrentNodeInSequence < Sequence.Length)
		{
			NextNode = Sequence[CurrentNodeInSequence];
			CurrentNodeInSequence++;
			Status = status.Success;
		}
		else
		{
			NextNode = RealNextNode;
			CurrentNodeInSequence = 0;
			Status = status.Success;
		}
		
		Owner.MoveToNextNode();
		return;
	}


}
