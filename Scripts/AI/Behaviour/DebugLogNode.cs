﻿using UnityEngine;
using System.Collections;

public class DebugLogNode : BehaviourNode
 {
	public string message = "test";
	
	
	public DebugLogNode(int NID,int NNode,BehaviourTree owner,string me) : base(NID,NNode,owner)
	{
		message = me;
	}
	
	
	public override void Run()
	{
	Debug.Log(message);
	Status = status.Success;
	Owner.MoveToNextNode();
	}
}
