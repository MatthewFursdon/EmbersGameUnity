﻿using UnityEngine;
using System.Collections;

public class SetMovementStateNode : BehaviourNode {

	public int MovementState;

	public SetMovementStateNode(int NID,int NNode,BehaviourTree owner,int MS) : base(NID,NNode,owner)
	{
		MovementState = MS;
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		
		if(OwnerNPC != null)
		{
			OwnerNPC.SetMovementState(MovementState);
			Status = status.Success;
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
