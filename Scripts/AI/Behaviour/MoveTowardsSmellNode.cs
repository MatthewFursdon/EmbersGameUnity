﻿using UnityEngine;
using System.Collections;

public class MoveTowardsSmellNode : BehaviourNode {



	public MoveTowardsSmellNode(int NID,int NNode,BehaviourTree owner) : base(NID,NNode,owner)
	{
	
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		if(OwnerNPC != null)
		{
			if(OwnerNPC.CurrentSmell != null)
			{
				OwnerNPC.TargetPosition = OwnerNPC.CurrentSmell.transform.position;
				OwnerNPC.Awake = true;
				OwnerNPC.ChangePath();
				Status = status.Success;
			}
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
