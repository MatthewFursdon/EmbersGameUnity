﻿using UnityEngine;
using System.Collections;

public class WanderNode  : BehaviourNode {



	public WanderNode(int NID,int NNode,BehaviourTree owner) : base(NID,NNode,owner)
	{
	
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();

		if(OwnerNPC != null)
		{
			OwnerNPC.TargetPosition = OwnerNPC.transform.position + new Vector3(Random.Range(-50,50),0,Random.Range(-50,50));
			OwnerNPC.Awake = true;
			OwnerNPC.ChangePath();
			OwnerNPC.SetMovementState(0);
			Status = status.Success;
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
