﻿using UnityEngine;
using System.Collections;

public class DelayNode : BehaviourNode {

	int Delay;
	int i = 0;
	public DelayNode(int NID,int NNode,BehaviourTree owner,int delay) : base(NID,NNode,owner)
	{
		Delay = delay;
	}
	
	public override void Tick()
	{
		if(i > Delay)
		{
			i = 0;
			Status = status.Success;
			Owner.MoveToNextNode();
		}
		i++;
	}
	
	public override void Run()
	{
		Status = status.Running;
		i = 0;
		
	}
}
