﻿using UnityEngine;
using System.Collections;

public class RandomValueNode : BehaviourNode {

	int Percentage;

	public RandomValueNode(int NID,int NNode,BehaviourTree owner,int percentage) : base(NID,NNode,owner)
	{
		Percentage = percentage;
	}
	
	
	
	public override void Run()
	{
		if(Random.Range(0,100) < Percentage)
		{
			Status = status.Success;
			//Debug.Log("random true");
		}
		else
		{
			//Debug.Log("random false");
			Status = status.Faliure;
		}
		
		Owner.MoveToNextNode();
	}
}
