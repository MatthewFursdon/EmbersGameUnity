﻿using UnityEngine;
using System.Collections;

public class CanAttackNode  : BehaviourNode {



	public CanAttackNode(int NID,int NNode,BehaviourTree owner) : base(NID,NNode,owner)
	{
		
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		
		if(OwnerNPC != null)
		{
			if(OwnerNPC.Prey != null)
			{
				if( OwnerNPC.CanAttack() && (Owner.transform.position - OwnerNPC.Prey.transform.position).magnitude < OwnerNPC.AttackRange)
				{
				Status = status.Success;
				}
				else
				{
				Status = status.Faliure;
				}
			}
			else
			{
				Status = status.Faliure;
			}
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
