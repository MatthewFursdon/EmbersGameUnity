﻿using UnityEngine;
using System.Collections;

public class IsUnderAttackNode  : BehaviourNode {



	public IsUnderAttackNode(int NID,int NNode,BehaviourTree owner) : base(NID,NNode,owner)
	{
		
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		
		if(OwnerNPC != null)
		{
			if(OwnerNPC.LastAttacker != null && OwnerNPC.Prey != OwnerNPC.LastAttacker)
			{
				
				Status = status.Success;
			}
			else
			{
				Status = status.Faliure;
			}
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
