﻿using UnityEngine;
using System.Collections;

public class MoveTowardsPreyNode : BehaviourNode {



	public MoveTowardsPreyNode(int NID,int NNode,BehaviourTree owner) : base(NID,NNode,owner)
	{
	
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		//Debug.Log(OwnerNPC.IsTraveling());
		if(OwnerNPC != null)
		{
			if(OwnerNPC.Prey != null)
			{
				if(OwnerNPC.path != null)
				{
					if( (OwnerNPC.path.vectorPath[OwnerNPC.path.vectorPath.Count-1]-OwnerNPC.Prey.transform.position).magnitude > 3)
					{
						OwnerNPC.TargetPosition = OwnerNPC.Prey.transform.position;
						OwnerNPC.Awake = true;
						OwnerNPC.ChangePath();
					}
				
				}
				else
				{
					OwnerNPC.TargetPosition = OwnerNPC.Prey.transform.position;
					OwnerNPC.Awake = true;
					OwnerNPC.ChangePath();
				}
				
				Status = status.Success;
			}
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
