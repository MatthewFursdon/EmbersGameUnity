﻿using UnityEngine;
using System.Collections;

public class SequenceNode : BehaviourNode
{

	public int[] Sequence;
	int CurrentNodeInSequence = 0;
	int RealNextNode;
	public SequenceNode(int NID,int NNode,BehaviourTree owner,int[] seq) : base(NID,NNode,owner)
	{
		Sequence = seq;
		NextNode = Sequence[0];
		RealNextNode = NNode;
	}
	
	public override void Run()
	{
		if(CurrentNodeInSequence < Sequence.Length)
		{
			NextNode = Sequence[CurrentNodeInSequence];
			CurrentNodeInSequence++;
			Status = status.Success;
		}
		else
		{
			NextNode = RealNextNode;
			CurrentNodeInSequence = 0;
			Status = status.Success;
		}
		
		Owner.MoveToNextNode();
	}


}
