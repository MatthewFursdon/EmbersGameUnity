﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BehaviourTree : MonoBehaviour {

public BehaviourNode[] Nodes = new BehaviourNode[60];
int CurrentNode = 0;
public Dictionary<string,object> Variables = new Dictionary<string,object>();
public NPC Owner;
	void Awake()
	{
		
		Owner = GetComponent<NPC>();
	
		
		Nodes[0] = new InverseConditionalSequenceNode(0,1,this,new int[5]{55,45,1,31,17});
		
		// hunt visible prey
		Nodes[1] = new CanSeePreyNode(1,2,this);
		Nodes[2] = new BranchNode(2,3,this,1,3,0);
		Nodes[3] = new IsTravelingNode(3,5,this);
		Nodes[4] = new BranchNode(4,5,this,3,0,5);
		Nodes[5] = new SetMovementStateNode(5,6,this,1);
		Nodes[6] = new MoveTowardsPreyNode(6,0,this);
		
		
		//randomly wander
		Nodes[17] = new IsTravelingNode(17,18,this);
		Nodes[18] = new BranchNode(18,-1,this,17,0,19);
		Nodes[19] = new RandomValueNode(19,20,this,5);
		Nodes[20] = new BranchNode(20,-1,this,19,22,23);
		//Nodes[21] = new SetMovementStateNode(21,22,this,0);
		Nodes[22] = new WanderNode(22,0,this);
		Nodes[23] = new SetMovementStateNode(23,0,this,2);
		//hunt down a smell
		
		Nodes[31] = new IsTravelingNode(31,32,this);
		Nodes[32] = new BranchNode(32,-1,this,31,0,33);
		Nodes[33] = new SetMovementStateNode(33,34,this,0);
		Nodes[34] = new MoveTowardsSmellNode(34,0,this);
		
		// attack
		
		Nodes[45] = new CanAttackNode(45,47,this);
		Nodes[47] = new BranchNode(47,-1,this,45,48,0);
		Nodes[48] = new AttackNode(48,0,this);
		//Nodes[49] = new SetMovementStateNode(49,0,this,2);
		
		Nodes[55] = new IsUnderAttackNode(55,56,this);
		Nodes[56] = new SetAttackerToPreyNode(56,45,this);
		
		
		
	}
	
	public void MoveToNextNode()
	{
		CurrentNode = Nodes[CurrentNode].NextNode;
		Nodes[CurrentNode].Status = BehaviourNode.status.Stopped;
		//Nodes[CurrentNode].Run();
	}
	
	void Update()
	{
		if(!Owner.Dead)
		{
		
		if(Nodes[CurrentNode] != null)
		{
			if(Nodes[CurrentNode].Status != BehaviourNode.status.Running)
			{	
				Nodes[CurrentNode].Run();
			}
			else if(Nodes[CurrentNode].Status == BehaviourNode.status.Running)
			{
				Nodes[CurrentNode].Tick();
			}
			else
			{
				//Nodes[CurrentNode].Status = BehaviourNode.status.Stopped;
				CurrentNode = Nodes[CurrentNode].NextNode;
			}
			
		}
		
		}
	}
	
	public void SetVariable(string name,object val)
	{
		Variables[name] = val;
	}

}
