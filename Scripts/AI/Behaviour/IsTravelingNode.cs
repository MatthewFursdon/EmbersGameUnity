﻿using UnityEngine;
using System.Collections;

public class IsTravelingNode : BehaviourNode {



	public IsTravelingNode(int NID,int NNode,BehaviourTree owner) : base(NID,NNode,owner)
	{
	
	}
	
	public override void Run()
	{
		
		NPC OwnerNPC =(NPC)Owner.gameObject.GetComponent<NPC>();
		
		if(OwnerNPC != null)
		{
			if(OwnerNPC.IsTraveling())
			{
				Status = status.Success;
			}
			else
			{
				Status = status.Faliure;
			}
		}
		else
		{
			Status = status.Faliure;
		}
		
		
		
		Owner.MoveToNextNode();
	}
}
