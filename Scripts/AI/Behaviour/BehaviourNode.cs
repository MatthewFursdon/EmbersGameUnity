﻿using UnityEngine;
using System.Collections;

public class BehaviourNode 
{
	
	public int NodeID;
	public int NextNode;
	public BehaviourTree Owner;
	public enum status{Success,Faliure,Running,Stopped};
	public status Status = status.Stopped;
	public BehaviourNode(int NID,int NNode,BehaviourTree owner)
	{
		NodeID = NID;
		NextNode = NNode;
		Owner = owner;
	}
	
	
	public virtual void Run()
	{
		Owner.MoveToNextNode();
		
	}
	
	public virtual void Tick()
	{
	
	//do stuff, on a tick-by-tick basis
	
	//node is done...
	
	
	}
	
}
