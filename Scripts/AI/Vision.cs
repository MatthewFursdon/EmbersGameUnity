﻿using UnityEngine;
using System.Collections;

public class Vision : MonoBehaviour 
{

	public LayerMask VisionMask;
	public LayerMask VisionRayMask;

	public NPC Owner;
	public void Start()
	{
	
	}
	
	public Vector3 RandomPointInBounds(GameObject Object)
	{
		Vector3 RandPoint = new Vector3(Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f),Random.Range(-1.0f,1.0f));
		RandPoint = Object.transform.TransformPoint(RandPoint * 0.5f);
		return RandPoint;
	}

	int i = 0;
	void OnTriggerStay(Collider col)
	{
		if (i > 500) {
			OnTriggerEnter (col);
			i = 0;
		}
		i++;
	}
	
	 /// <summary>
 /// Checks if a GameObject is in a LayerMask
 /// </summary>
 /// <param name="obj">GameObject to test</param>
 /// <param name="layerMask">LayerMask with all the layers to test against</param>
 /// <returns>True if in any of the layers in the LayerMask</returns>
 private bool IsInLayerMask(GameObject obj, LayerMask layerMask)
 {
     // Convert the object's layer to a bitfield for comparison
     int objLayerMask = (1 << obj.layer);
     if ((layerMask.value & objLayerMask) > 0)  // Extra round brackets required!
         return true;
     else
         return false;
 }
	
	void OnTriggerEnter(Collider col)
	{
	

	GameObject collided = col.transform.root.gameObject;
	
	if(IsInLayerMask(col.transform.gameObject,VisionMask))
	{

			Debug.Log("might be able to see something" + collided.name);
	
			float VisionRange;

			if(Global.TimeManager.IsSunUp)
			{
				VisionRange = Owner.DayVisionRange;
			}
			else
			{
				VisionRange = Owner.NightVisionRange;
			}

		// use raycast to see if we can really see, or if there's a building in the way
			int i = 0;
			Vector3 Origion = transform.position + new Vector3(0,2,0);
			
			while(i <= 5)
			{
			Vector3 RayTo = collided.GetComponent<Collider>().bounds.center;//RandomPointInBounds(collided);
			
			Vector3 Direction = (RayTo-Origion);
			float Distance = Direction.magnitude;
			Direction.Normalize();
			
			RaycastHit results;
			
			Physics.Raycast(Origion,Direction,out results,Mathf.Infinity);
			
			if(results.collider != null)
			{
			
					Debug.DrawLine(Origion,results.point,Color.red,30.0f,true);

			Player P = results.collider.transform.root.GetComponent<Player>();
			NPC N = results.collider.transform.root.GetComponent<NPC>();


				if(P != null)
				{
						float range = Vector3.Distance(transform.position,P.transform.position);

						Debug.Log( range + " " + VisionRange);
						if(range < VisionRange)
						{
							Owner.Prey = P.gameObject; 
							break;
						}
				}
				
			if(N != null)
			{
				if(N.Prey != null)
				{
					Owner.Prey = N.Prey;
				}
			}
			
			 i++;
			}


			return;

			}
	
	}
	
	}
	
}
