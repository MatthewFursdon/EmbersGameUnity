﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using Pathfinding.RVO;
public class NPC : MonoBehaviour {

public CharacterController controller;
	// Use this for initialization

	public AnimationClip Walking;
	public AnimationClip Running;
	public AnimationClip Idle;
	public Animation Animation;
	public Path path;
	
	public Smell CurrentSmell;
	public GameObject Prey;
	
	public SkinnedMeshRenderer SkinnedMeshRenderer;
	
	public Vector3 TargetPosition = new Vector3(-1,-1,-1);
	public float AttackRange = 5.0f;
	
	public Player LastAttacker;
	public DamageTaker DamageTaker;
	
	public bool Dead;
	public LayerMask DestructionMask;
	public LayerMask  SmellMask;
	
	public float AttackDelay = 1.0f;
	float SwingRecoverTime = 0;
	//public RVOController Controller;

	public float DayVisionRange;
	public float NightVisionRange;

	public float AttackBonus = 5;
	
	public List<MovementState> MovementStates = new List<MovementState>();
	MovementState currentMS;
	void OnTriggerEnter(Collider col)
	{
		//if(col.gameObject.layer == SmellMask)
		//{
			Smell NewSmell = (Smell)col.gameObject.GetComponent<Smell>();
			if(NewSmell != null && (CurrentSmell == null || NewSmell.Interest > CurrentSmell.Interest))
			{
				Debug.Log("Smelled something interesting " + col.gameObject.name);
				CurrentSmell = NewSmell;
		
			}
			
		//}
	}

	
	void OnTriggerStay(Collider col)
	{
		//if(col.gameObject.layer == SmellMask)
		//{
			Smell NewSmell = (Smell)col.gameObject.GetComponent<Smell>();
			if(NewSmell != null &&  (CurrentSmell == null || NewSmell.Interest > CurrentSmell.Interest))
			{
				Debug.Log("Smelled something interesting " + NewSmell);
				CurrentSmell = NewSmell;
		
			}
			
		//}
	}
	
	
	public void Attack(GameObject target)
	{
		if(CanAttack())
		{
		
		transform.rotation = Quaternion.LookRotation(target.transform.position-transform.position);
		Animation.Play("Attack_1");
		
		DamageTaker D = target.GetComponent<DamageTaker>();
		
		if(D != null)
		{
			D.TakeDamage(Item.DamageTypes.Smashing,Random.Range(5,30),Random.Range(0,10)+AttackBonus);
		}
		
		SwingRecoverTime = Time.time + AttackDelay;
		
		}
	}	
	
	public void SeenPrey(GameObject prey)
	{
	Prey = prey;
	}
	
	public bool Awake = false;
	
	public bool CanAttack()
	{
		if(SwingRecoverTime < Time.time)
		{
			return true;
		}
		return false;
	
	}
	
	
	public void SetMovementState(int MS)
	{
		currentMS = MovementStates[MS];
			if (IsTraveling()) {

			Animation.Play (currentMS.Animation);
		}
	}
	
	void Start () 
	{
	
	InvokeRepeating("WakeUp",0,1);
	InvokeRepeating("RecalculateCurrentChunk",0,1);
	
	Animation.CrossFade("idle_1");
	SetMovementState(0);
	}
	
	void RecalculateCurrentChunk()
	{
		Vector3 pos = transform.position;
		float x = Mathf.Floor(pos.x/(Block.BLOCK_SIZE*Chunk.CHUNK_SIZE));
		float z = Mathf.Floor(pos.z/(Block.BLOCK_SIZE*Chunk.CHUNK_SIZE));
		
		//Debug.Log(x + " " + z);
		Vector2 v = new Vector2(x,z);
		

		
		if(World.Chunks.ContainsKey(v))
		{
		
		Chunk C = World.Chunks[new Vector2(x,z)];
		
		if(C.Visible && Awake)
		{
			SkinnedMeshRenderer.enabled = true;
		}
		else
		{
			SkinnedMeshRenderer.enabled = false;
		}
	
		}
	}

    int CurrentWaypoint = 0;
	public Vector3 WaypointPosition = Vector3.zero;

    public void OnPathComplete (Path p) 
	{
		CurrentWaypoint = 0;
		CalulatingPath = false;
		if(p.vectorPath.Count > 0)
		{
			path = p;
			WaypointPosition = p.vectorPath[CurrentWaypoint];	
		}
		else
		{
		path = null;
		}
		
	}

	
	Vector3 nul = new Vector3(-1,-1,-1);
	
	bool CalulatingPath;
	public void ChangePath()
	{
		if(TargetPosition != nul)
			{
				Seeker seeker = GetComponent<Seeker>();
				seeker.StartPath (transform.position,TargetPosition, OnPathComplete);
				CalulatingPath = true;
			}
		
	CurrentWaypoint = 0;
	}
	
	
	public void Die()
	{
		Animation.Play("Dead_2_Falling_Back");
		Dead = true;
		MonoBehaviour.Destroy (this.GetComponent<CharacterController> ());
		MonoBehaviour.Destroy (this.GetComponent<CapsuleCollider> ());
	}
	
	public void Damaged(Item.DamageTypes DamageType,float Damage)
	{
		//damage already taken at this point
		
		if(DamageTaker.health <= 0)
		{
			Die();
		}
		
	}
	
	void WakeUp()
	{
		if((Global.Player.transform.position - transform.position).magnitude < 300)
		{
			Awake = true;
		}
		else
		{
			Awake = false;
		}
	
	}
	
	bool findingNewPath = false;

	
	public bool IsTraveling()
	{
		return (path != null || CalulatingPath);
	}
	
	Quaternion FinalRot;
	
	void Update () 
	{
	
		if(!Dead)
		{
		
		if(Prey != null)
		{
			if((Prey.transform.position - transform.position).magnitude > 100 || !Prey.active)
			{
				Prey = null;
			}
		
		}
	
	if(Awake)
	{
	

	controller.SimpleMove(new Vector3(0,-9.81f,0));
	//Controller.Move(new Vector3(0,-9.81f,0));
	
	if(path != null)
	{
	
	
			if(CurrentWaypoint >= path.vectorPath.Count || path.vectorPath[CurrentWaypoint].y - transform.position.y > 3)
			{
				path = null;
				return;
			}
			
	
	
		transform.rotation = Quaternion.Slerp(transform.rotation,FinalRot,Time.deltaTime*4f);
	      Vector3 dir = (path.vectorPath[CurrentWaypoint]-transform.position).normalized;
	   dir *= currentMS.Speed * Time.fixedDeltaTime;
        controller.SimpleMove (dir);
       // Controller.Move (dir);
		

		if(Animation.clip.name != currentMS.Animation)
		{
			Animation.Play(currentMS.Animation);
		}
	
		 if (Vector3.Distance (transform.position,path.vectorPath[CurrentWaypoint]) < 5)
		 {
		 
		 
				
		 
				CurrentWaypoint++;
			if(CurrentWaypoint < path.vectorPath.Count)
			{
				
			
				Vector3 Target = path.vectorPath[CurrentWaypoint];
				Vector3 TargetLook = new Vector3(Target.x,transform.position.y,Target.z);
				FinalRot = Quaternion.LookRotation(TargetLook-transform.position);
				
				RaycastHit hitinfo;
				if(Physics.Linecast(path.vectorPath[CurrentWaypoint-1]+ new Vector3(0,2,0),Target + new Vector3(0,2,0),out hitinfo,DestructionMask))
				{
					Debug.Log("gonna have to destroy a door");
					
					
					
					DamageTaker D = hitinfo.collider.GetComponentInParent<DamageTaker>();
					
					if(D != null)
					{
						
						if(this.Prey != null)
						{
							this.Prey = D.gameObject;
						}
						else
						{
							path = null;
						}
					}
					
					//GameObject O = hitinfo.collider.transform.root.gameObject;
					
					//DamageTaker D = O.GetComponent<DamageTaker>();
					//if(D != null)
					//{
						//this.Prey = O;
					//}
				
				}
				
	
				
				
				
			}

            return;
		}
		
		}
		
		}
	}
	}
	
}

