﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class NPC_Spawner : MonoBehaviour {

	public List<GameObject> SpawnList = new List<GameObject>();
	public int MaxNPCs;
	public int SpawnChance = 10;
	public int StayChance = 2;
	public int npcsspawned = 0;
	// Use this for initialization
	void Start () 
	{
	
		if(Random.Range(0,1000) > StayChance)
		{
		Object.Destroy(this.gameObject);
		}
	
	InvokeRepeating("TrySpawn",Random.value*2.0f,1);
	}
	
	
	void TrySpawn()
	{
		if(npcsspawned < 2)
		{
			if(Random.Range(0,100) <= SpawnChance)
			{
			int i = Random.Range(0,SpawnList.Count);
			Global.NPCManager.SpawnNPC(SpawnList[i],transform.position);
				npcsspawned++;

			}
		
		}
	
	}
}
