using System;
using System.Collections.Generic;
using UnityEngine;

public class NPCManager : MonoBehaviour
{
	public List<GameObject> NPCPrefabs = new List<GameObject>();

	public List<NPC> NPCs = new List<NPC>();

	public int SpawnedNPCs;

	public static int DespawnDistance = 100;

	public int DsDS = NPCManager.DespawnDistance ^ 2;
	public GameObject SmellPrefab;
	private void Awake()
	{
		Global.NPCManager = this;
	}

	private void Update()
	{
	}

	public void SpawnNPCNoise(Vector3 position,float hearingrange,float intrest)
	{
		GameObject gameObject = (GameObject)GameObject.Instantiate(SmellPrefab, position, Quaternion.identity);
		gameObject.GetComponentInChildren<Smell> ().transform.localScale = new Vector3 (hearingrange, hearingrange, hearingrange);
	}

	public bool SpawnNPC(GameObject i, Vector3 pos)
	{
		GameObject gameObject = (GameObject)GameObject.Instantiate(i, pos, Quaternion.identity);
		this.NPCs.Add(gameObject.GetComponent<NPC>());
		this.SpawnedNPCs = this.NPCs.Count;
		return true;
	}

	public bool SpawnNPC(int i, Vector3 pos)
	{
		GameObject gameObject = (GameObject)GameObject.Instantiate(this.NPCPrefabs[(i)], pos, Quaternion.identity);
		this.NPCs.Add(gameObject.GetComponent<NPC>());
		this.SpawnedNPCs = this.NPCs.Count;
		return true;
	}

	/*
	public void DespawnNPCs()
	{
		List<NPC> list = new List<NPC>();
		
		foreach(NPC N in NPCs)
		{
				if ((N.transform.position - Global.Player.transform.position).sqrMagnitude() > (float)this.DsDS)
				{
					list.Add(current);
				}
		}
	
		int i = 0;
		while(i < list.Count)
		{

				NPC current2 = enumerator2.get_Current();
				this.NPCs.Remove(NPCs);
				Object.Destroy(current2.get_gameObject());
			}
		}
		this.SpawnedNPCs = this.NPCs.get_Count();
		}
	}
	*/
	
}
